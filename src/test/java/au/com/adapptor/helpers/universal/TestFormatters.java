package au.com.adapptor.helpers.universal;

import org.junit.Test;
import org.junit.Before;
import org.junit.After;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import static org.junit.Assert.*;

/**
 * A test class for class {@link Formatters}.
 */
@SuppressWarnings("ConstantConditions")
public class TestFormatters {
    private Calendar cal;

    @Before
    public void setUp() {
        cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, 2016);
        cal.set(Calendar.MONTH, Calendar.MARCH);
        cal.set(Calendar.DATE, 7);
        cal.set(Calendar.HOUR_OF_DAY, 2);
        cal.set(Calendar.MINUTE, 4);
        cal.set(Calendar.SECOND, 8);
        cal.set(Calendar.MILLISECOND, 0);
    }

    @After
    public void tearDown() {
        cal = null;
    }

    @Test
    public void testFullDateFormat() throws ParseException {
        final SimpleDateFormat sdf = Formatters.createSimpleDateFormatter(Formatters.kFullDateFormat);
        final String date = sdf.format(cal.getTime());

        assertEquals("07 Mar 2016", date);
        sdf.parse(date);
    }

    @Test
    public void testFullDateFormatWithDayOfWeek() throws ParseException {
        final SimpleDateFormat sdf = Formatters.createSimpleDateFormatter(Formatters.kFullDateFormatWithDayOfWeek);
        final String date = sdf.format(cal.getTime());

        assertEquals("Mon 07 Mar 2016", date);
        sdf.parse(date);
    }

    @Test
    public void test24HourTimeFormat() throws ParseException {
        final SimpleDateFormat sdf = Formatters.createSimpleDateFormatter(Formatters.k24HourTimeFormat);
        final String date = sdf.format(cal.getTime());

        assertEquals("02:04", date);
        sdf.parse(date);
    }

    @Test
    public void testFullDateTimeFormat() throws ParseException {
        final SimpleDateFormat sdf = Formatters.createSimpleDateFormatter(Formatters.kFullDateTimeFormat);
        final String date = sdf.format(cal.getTime());

        assertEquals("07 Mar 2016 02:04", date);
        sdf.parse(date);
    }

    @Test
    public void testFormatDateWithString() {
        final String nullString = Formatters.formatDate("EEE MM dd HH:mm:ss", null);
        final String date = Formatters.formatDate("EEE MMM dd HH:mm:ss", cal);

        assertNull(nullString);
        assertEquals("Mon Mar 07 02:04:08", date);
    }

    @Test
    public void testFormatDateWithDateFormat() {
        final SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss", TimeHelpers.getPerthLocale());
        final String nullString = Formatters.formatDate(sdf, null);
        final String date = Formatters.formatDate(sdf, cal);

        assertNull(nullString);
        assertEquals("Mon Mar 07 02:04:08", date);
    }
}
