package au.com.adapptor.helpers.universal;

import org.junit.Test;

import io.reactivex.functions.Action;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * A test class for class {@link MiscHelpers}.
 */
public class TestMiscHelpers {
    @Test
    public void testCompareVersionStrings() {
        assertTrue(MiscHelpers.compareVersionStrings("1.0.3", "1.0.3") == 0);
        assertTrue(MiscHelpers.compareVersionStrings("1.0.4", "1.0.5") < 0);
        assertTrue(MiscHelpers.compareVersionStrings("1.0.5", "1.0.4") > 0);
        assertTrue(MiscHelpers.compareVersionStrings("1.0.5", "1.0") > 0);
        assertTrue(MiscHelpers.compareVersionStrings("1.0", "1.0.5") < 0);
        assertTrue(MiscHelpers.compareVersionStrings("1.0", "3.3b") < 0);
        assertTrue(MiscHelpers.compareVersionStrings("1.0", "0.9.3b") > 0);
        assertTrue(MiscHelpers.compareVersionStrings(".99", ".999") < 0);
        assertTrue(MiscHelpers.compareVersionStrings("2", "02") == 0);
        assertTrue(MiscHelpers.compareVersionStrings("23", "023") == 0);
        assertTrue(MiscHelpers.compareVersionStrings("6.7.8.9", "06.007.0008.00009") == 0);
        assertTrue(MiscHelpers.compareVersionStrings("0.5.0", "0.6.!") < 0);

        // Error cases.
        assertThrows(() -> MiscHelpers.compareVersionStrings("1.0", "1..0"));
        assertThrows(() -> MiscHelpers.compareVersionStrings("1.0", "1..0"));
        assertThrows(() -> MiscHelpers.compareVersionStrings("1.0", "a"));
        assertThrows(() -> MiscHelpers.compareVersionStrings("1.3b", "1.0"));
        assertThrows(() -> MiscHelpers.compareVersionStrings(".1", "0.2"));
        assertThrows(() -> MiscHelpers.compareVersionStrings("1.0", null));
        assertThrows(() -> MiscHelpers.compareVersionStrings(null, "1.0"));
        assertThrows(() -> MiscHelpers.compareVersionStrings(null, null));
        assertThrows(() -> MiscHelpers.compareVersionStrings("Hello", "World"));
    }

    /**
     * Checks that the specified action throws an exception.
     * If it does not, raises a test failure.
     * @param action the action to be performed
     */
    private void assertThrows(Action action) {
        try {
            action.run();
            fail("Expected test to throw an exception, but it didn't.");
        } catch (Exception e) {
            // Very good!
        }
    }
}
