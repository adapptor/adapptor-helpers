package au.com.adapptor.helpers.universal;

import org.junit.Test;

import java.util.Calendar;
import java.util.Locale;
import java.util.TimeZone;

import static org.junit.Assert.*;

/**
 * A test class for class {@link TimeHelpers}.
 */
public class TestTimeHelpers {
    @Test
    public void testPastFutureTime() {
        final Calendar today = Calendar.getInstance(TimeZone.getTimeZone("Australia/Perth"), new Locale("en", "AU"));
        today.set(Calendar.HOUR, 0);
        today.set(Calendar.MINUTE, 0);
        today.set(Calendar.SECOND, 0);
        today.set(Calendar.MILLISECOND, 0);

        final Calendar now = TimeHelpers.getPerthCalendar(today.get(Calendar.YEAR), today.get(Calendar.MONTH)+1, today.get(Calendar.DATE));
        assertEquals("getCurrentPerthCalendar didn't return the correct day", now, today);

        final Calendar tomorrow = (Calendar) now.clone();
        tomorrow.add(Calendar.DATE, 1);

        final Calendar yesterday = (Calendar) now.clone();
        yesterday.add(Calendar.DATE, -1);

        assertTrue("Tomorrow is not in the future", TimeHelpers.timeIsInTheFuture(tomorrow, now));
        assertFalse("Yesterday is in the future", TimeHelpers.timeIsInTheFuture(yesterday, now));
        assertFalse("Now is in the future", TimeHelpers.timeIsInTheFuture(now, now));

        assertFalse("Tomorrow is in the past", TimeHelpers.timeIsInThePast(tomorrow, now));
        assertTrue("Yesterday is not in the past", TimeHelpers.timeIsInThePast(yesterday, now));
        assertFalse("Now is in the past", TimeHelpers.timeIsInThePast(now, now));
    }

    /**
     * Tests the function which creates a 12-hour time string from 24 hour time
     */
    @Test
    public void test12HourTimeString() {
        assertTrue("Expected time to read 12:00AM",
            TimeHelpers.getTwelveHourTimeString(0, 0).equals("12:00AM"));
        assertTrue("Expected time to read 12:00PM",
            TimeHelpers.getTwelveHourTimeString(12, 0).equals("12:00PM"));
        assertTrue("Expected time to read 1:30AM",
            TimeHelpers.getTwelveHourTimeString(1, 30).equals("1:30AM"));
        assertTrue("Expected time to read 1:30PM",
            TimeHelpers.getTwelveHourTimeString(13, 30).equals("1:30PM"));
    }
}

