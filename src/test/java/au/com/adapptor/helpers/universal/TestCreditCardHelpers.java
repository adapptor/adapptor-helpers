package au.com.adapptor.helpers.universal;

import org.junit.Assert;
import org.junit.Test;

/**
 * Tests for credit card helper functions
 */
public class TestCreditCardHelpers {

    @Test
    public void testInvalidCreditCardString() {
        String creditCardAllNumbers = "5541691640960994"; //valid master card
        String creditCard1Letter = "554169164096099A";
        String creditCardSpecialChar = "554169164096099*";

        Assert.assertTrue("Expected card with all numbers to be valid",
            CreditCardHelpers.validateCreditCard(creditCardAllNumbers)
        != CreditCardHelpers.CreditCardType.INVALID);

        Assert.assertTrue("Expected card with letter to be invalid",
            CreditCardHelpers.validateCreditCard(creditCard1Letter)
            == CreditCardHelpers.CreditCardType.INVALID);

        Assert.assertTrue("Expected card with * to be invalid",
            CreditCardHelpers.validateCreditCard(creditCardSpecialChar)
            == CreditCardHelpers.CreditCardType.INVALID);
    }

    @Test
    public void testMasterCardNumbers() {

        String[] validMasterCards = {
            "5446825206739576",
            "5347242074881219",
            "5443936453025529",
            "5227354364893997",
            "5142295391200056",
            "5479493119276366",
            "5484746598506626",
            "5327765684459251",
            "5160728486937247",
            "5328393256992705"
        };

        //card numbers with MasterCard IIN and length but fail the Luhn Check
        String[] invalidMasterCards = {
            "5446825206739577",
            "5347242074881218",
            "5443936453025520",
            "5227354364893998",
            "5142295391200057",
            "5479493119276367",
            "5484746598506627",
            "5327765684459252",
            "5160728486937248",
            "5328393256992706"
        };

        testCardValidation(validMasterCards, CreditCardHelpers.CreditCardType.MASTERCARD);
        testCardValidation(invalidMasterCards, CreditCardHelpers.CreditCardType.INVALID);
    }

    @Test
    public void testAmexCardNumbers() {

        String[] validAmexCards = {
            "379117054095091",
            "378251823013852",
            "370011843030064",
            "373256672807424",
            "376989543386080",
            "376055880944723",
            "341312042366705",
            "348288735382220",
            "343424915442360",
            "376292214586124"
        };

        //card numbers with AMEX IIN and length but fail the Luhn Check
        String[] invalidAmexCards = {
            "379117054096091",
            "378251823016852",
            "370011847030064",
            "373256472807424",
            "376989543686080",
            "376055880344723",
            "341312045366705",
            "348288735342220",
            "343424915448360",
            "376292214526124"
        };

        testCardValidation(validAmexCards, CreditCardHelpers.CreditCardType.AMERICAN_EXPRESS);
        testCardValidation(invalidAmexCards, CreditCardHelpers.CreditCardType.INVALID);
    }

    @Test
    public void testDinersCardCardNumbers() {

        String[] validDcCards = {
            "36041093787754",
            "38646466041908",
            "30264234247746",
            "38711480674355",
            "38715185920914",
            "36383800983445",
            "30228780736329",
            "30389713525021",
            "38606087069191",
            "30349041407128"
        };

        //card numbers with Diners Club IIN and length but fail the Luhn Check
        String[] invalidDcCards = {
            "36041093787756",
            "38646466041918",
            "30264234247646",
            "38711480654355",
            "38715184920914",
            "36383300983445",
            "30228580736329",
            "30386713525021",
            "38606587069191",
            "30349441407128"
        };

        testCardValidation(validDcCards, CreditCardHelpers.CreditCardType.DINERS_CLUB_INTERNATIONAL);
        testCardValidation(invalidDcCards, CreditCardHelpers.CreditCardType.INVALID);
    }

    @Test
    public void testVisaCardNumbers() {

        String[] validVisaCards = {
            "4929000093076032",
            "4916363560669514",
            "4421861817720962",
            "4929994394921294",
            "4929640692762209",
            "4556555584746716",
            "4556991150023844",
            "4024007114401101",
            "4716934043401915",
            "4485776696168526"
        };

        //card numbers with VISA IIN and length but fail the Luhn Check
        String[] invalidVisaCards = {
            "4929000093077032",
            "4916363660669514",
            "4421831817720962",
            "4929994394921295",
            "4929640792762209",
            "4557555584746716",
            "4556981150023844",
            "4026007114401101",
            "4717934043401915",
            "4489776696168526"
        };

        testCardValidation(validVisaCards, CreditCardHelpers.CreditCardType.VISA);
        testCardValidation(invalidVisaCards, CreditCardHelpers.CreditCardType.INVALID);
    }

    @Test
    public void testJCBCardNumbers() {

        String[] validJcbCards = {
            "3585380842835087",
            "3566704252546525",
            "3575341836720687",
            "3506165177362203",
            "3538578870467255",
            "3530353658203738",
            "3545613208412615",
            "3514238182048727",
            "3524703474516663",
            "3546617888545528"
        };

        //card numbers with JCB IIN and length but fail the Luhn Check
        String[] invalidJcbCards = {
            "3585380842835088",
            "3566704252546527",
            "3575341836724687",
            "3506165177332203",
            "3538578877568255",
            "3530373658203738",
            "3545613268412615",
            "3514238182058727",
            "3524703474514663",
            "3546617988545528"
        };

        testCardValidation(validJcbCards, CreditCardHelpers.CreditCardType.JCB);
        testCardValidation(invalidJcbCards, CreditCardHelpers.CreditCardType.INVALID);
    }

    /**
     * test that a list of credit card numbers match the expected type passed in
     * @param cardNumberStrings the credit card number strings to test
     * @param type the credit card type to test
     */
    private void testCardValidation(String[] cardNumberStrings,
                                    CreditCardHelpers.CreditCardType type) {

        for (String card : cardNumberStrings) {
            Assert.assertTrue("Expected " + card + " to be " + type +", but was " +
                    CreditCardHelpers.validateCreditCard(card),
                CreditCardHelpers.validateCreditCard(card) == type);
        }

    }

}
