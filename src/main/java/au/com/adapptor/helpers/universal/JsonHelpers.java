package au.com.adapptor.helpers.universal;

import com.google.common.base.Splitter;
import com.google.j2objc.annotations.ObjectiveCName;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.TimeZone;

/**
 * JsonHelpers: Contains methods for extracting elements from a JSON map, etc.
 */
@SuppressWarnings({"unused", "unchecked"})
public class JsonHelpers {
    /**  Convert a JSON-encoded object into a JSON object Map */
    public static Map<String, Object> mapFromJsonObject(String text) {
        try {
            Object obj = new JSONObject(text);
            return toMap((JSONObject) obj);
        } catch (Exception ex) {
            return null;
        }
    }

    /**  Convert a JSON-encoded list into a JSON object List */
    public static List<Object> listFromJsonArray(String text) {
        try {
            Object obj = new JSONArray(text);
            return toList((JSONArray) obj);
        } catch (Exception ex) {
            return null;
        }
    }

    public static Map<String, Object> mapFromJsonObject(InputStream is) {
        return mapFromJsonObject(stringFromInputStream(is));
    }

    public static List<Object> listFromJsonArray(InputStream is) {
        return listFromJsonArray(stringFromInputStream(is));
    }

    public static String stringFromInputStream(InputStream is) {
        return stringFromInputStream(is, "UTF-8");
    }

    public static String stringFromInputStream(InputStream is, String charsetName) {
        Scanner s = new Scanner(is, charsetName).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    /**  Return an object located under the period-delimited key path
     *
     * @param json JSON object Map
     * @param path period-delimited key path
     * @return object at path, or null if the path is invalid
     */
    public static Object getJsonObject(Map<String, Object> json, String path) {
        return getJsonObject(json, path, ".");
    }

    /**  Return an object located under the key path
     *
     * @param json JSON object Map
     * @param path separator-delimited key path
     * @param separator separator used in key path
     * @return object at path, or null if the path is invalid
     */
    public static Object getJsonObject(Map<String, Object> json, String path, String separator) {
        Object obj = json;

        for (String sub : Splitter.on(separator).split(path)) {
            if (obj == null || !(obj instanceof Map)) {
                return null;
            }

            obj = ((Map<String, Object>) obj).get(sub);
        }

        return obj;
    }

    /**  Return a String at the given path of a JSON object Map */
    public static String getJsonString(Map<String, Object> json, String key) {
        return nullCheckAndCast(getJsonObject(json, key));
    }

    /**  Return a Number at the given path of a JSON object Map */
    public static Number getJsonNumber(Map<String, Object> json, String key) {
        return nullCheckAndCast(getJsonObject(json, key));
    }

    /**  Return a Boolean at the given path of a JSON object Map */
    public static Boolean getJsonBoolean(Map<String, Object> json, String key) {
        return nullCheckAndCast(getJsonObject(json, key));
    }

    /**  Return a List object at the given path of a JSON object Map */
    public static <T> List<T> getJsonList(Map<String, Object> json, String key) {
        return nullCheckAndCast(getJsonObject(json, key));
    }

    /**  Return a Map object at the given path of a JSON object Map */
    @ObjectiveCName("getJsonMapWithMap:key:")
    public static Map<String, Object> getJsonMap(Map<String, Object> json, String key) {
        return nullCheckAndCast(getJsonObject(json, key));
    }

    /**  Return a List object at the given path of a JSON object Map */
    @ObjectiveCName("getJsonMapList:key:")
    public static List<Map<String, Object>> getJsonMapList(Map<String, Object> json, String key) {
        return nullCheckAndCast(getJsonObject(json, key));
    }

    /**  Return a Calendar from the date (String) at the given path of a JSON object Map */
    public static Calendar getJsonDate(Map<String, Object> json, String key) {
        return getJsonDate(json, key, null);
    }

    /**  Return a Calendar from the date (String) at the given path of a JSON object Map */
    public static Calendar getJsonDate(Map<String, Object> json, String key, TimeZone tz) {
        String date = getJsonString(json, key);
        return date != null ? TimeHelpers.dateFromString(date, tz) : null;
    }

    /**  Returns null if {@code obj} is null or JSONObject.NULL; else returns {@code (T) obj}. */
    private static <T> T nullCheckAndCast(Object obj) {
        return obj == null || obj.equals(JSONObject.NULL) ? null : (T) obj;
    }

    /**  Convert a JSONObject to a JSON object Map */
    static Map<String, Object> toMap(JSONObject object) throws JSONException {
        if (object == JSONObject.NULL) {
            return null;
        }

        Map<String, Object> map = new HashMap<>();
        Iterator it = object.keys();

        while (it.hasNext()) {
            String key = (String) it.next();
            Object value = object.get(key);

            map.put(key, (value instanceof JSONArray)  ? toList((JSONArray) value)
                : (value instanceof JSONObject) ? toMap((JSONObject) value)
                : value);
        }

        return map;
    }

    /**  Convert a JSONArray to a JSON object List */
    static List<Object> toList(JSONArray array) throws JSONException {
        if (array == JSONObject.NULL) {
            return null;
        }

        List<Object> list = new ArrayList<>();

        for (int i = 0; i < array.length(); i++) {
            Object value = array.get(i);

            list.add((value instanceof JSONArray)  ? toList((JSONArray) value)
                : (value instanceof JSONObject) ? toMap((JSONObject) value)
                : value);
        }

        return list;
    }
}
