package au.com.adapptor.helpers.universal;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * Encapsulates a latitude and longitude.
 * @author Adapptor
 */
@SuppressWarnings("unused")
public class Location implements Serializable {
    /** The latitude in degrees. */
    public double latitude;

    /** The longitude in degrees. */
    public double longitude;

    /**
     * Constructor.
     */
    public Location() {
    }

    /**
     * Constructor.
     * @param lat latitude in degrees
     * @param lon longitude in degrees
     */
    public Location(double lat, double lon) {
        latitude = lat;
        longitude = lon;
    }

    /**
     * Constructs a location based off a string representation of latitude and longitude.
     * @param latLongString a string formatted like: "-31.742797, 115.767523"
     * @throws IllegalArgumentException if the input is invalid
     */
    public Location(String latLongString) {
        final String[] tokens = latLongString.split(",");

        if (tokens.length != 2) {
            throw new IllegalArgumentException("Invalid lat/long: " + latLongString);
        }

        try {
            latitude = Double.parseDouble(tokens[0].trim());
            longitude = Double.parseDouble(tokens[1].trim());
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException("Invalid lat/long: " + latLongString, e);
        }
    }

    /**
     * Converts this object to a map representing its internal state.
     * @return key-value map representation.
     */
    public Map<String, Object> toMap() {
        final Map<String, Object> map = new HashMap<>();
        map.put("latitude", latitude);
        map.put("longitude", longitude);
        return map;
    }

    /**
     * Returns the latitude.
     * @return the latitude
     */
    public double latitude() {
        return latitude;
    }

    /**
     * Returns the longitude.
     * @return the longitude
     */
    public double longitude() {
        return longitude;
    }
}
