package au.com.adapptor.helpers.universal;

/**
 * A geo-fence with a name, defined by a radius at a lat/long location
 */
public class CircularGeofence  {

    /** The name of this area. */
    public String name;

    /** The latitude of this area. */
    public double latitude;

    /** The longitude of this area. */
    public double longitude;

    /** The radius of this area. */
    public int radius;
    
    public String name() {
        return name;
    }
    
    public double latitude() {
        return latitude;
    }
    
    public double longitude() {
        return longitude;
    }
    
    public int radius() {
        return radius;
    }
}

