package au.com.adapptor.helpers.universal;

import com.google.j2objc.annotations.ObjectiveCName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper functions to validate user inputs.
 */
@SuppressWarnings("unused")
public class InputValidationHelpers {
    /** A simple regex used for validating (most) email addresses. */
    private static final Pattern kEmailPattern =
        Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

    /**
     * Takes an email address string and validates that it is (in most cases) formatted correctly.
     * Does not verify that the email is an existing address.
     *
     * @param emailAddress the email address to validate the format of
     * @return true if the email address has valid formatting, false otherwise
     */
    @ObjectiveCName("isEmailFormatValid:")
    public static boolean isEmailFormatValid(String emailAddress) {
        Matcher matcher = kEmailPattern.matcher(emailAddress);
        return matcher.find();
    }
}
