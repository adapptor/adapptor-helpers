package au.com.adapptor.helpers.universal;

import com.google.j2objc.annotations.ObjectiveCName;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Contains formatters used to format dates and times as strings.
 * @author Adapptor
 */
@SuppressWarnings("unused")
public class Formatters {
    public static final String k12HourTimeFormat = "h:mm a";
    public static final String k24HourTimeFormat = "HH:mm";
    public static final String kShortDateFormat = "dd/MM/yyyy";
    public static final String kShortDateTimeFormat = "dd/MM/yyyy HH:mm";
    public static final String kFullDateFormat = "dd MMM yyyy";
    public static final String kFullDateTimeFormat = "dd MMM yyyy HH:mm";
    public static final String kFullDateFormatWithDayOfWeek = "EEE dd MMM yyyy";
    public static final String kFilePathDateTimeFormat = "ddMMyyHHmm";

    /**
     * Creates a {@link SimpleDateFormat} using the default time zone.
     * @param format the format string
     * @return a {@link SimpleDateFormat} in the default time zone
     */
    public static SimpleDateFormat createSimpleDateFormatter(String format) {
        return new SimpleDateFormat(format, TimeHelpers.getPerthLocale());
    }

    /**
     * Creates a {@link SimpleDateFormat} using the specified time zone.
     * @param format the format string
     * @param tz the time zone
     * @return a {@link SimpleDateFormat} in the specified time zone
     */
    public static SimpleDateFormat createSimpleDateFormatter(String format, TimeZone tz) {
        final SimpleDateFormat sdf = new SimpleDateFormat(format, TimeHelpers.getPerthLocale());
        sdf.setTimeZone(tz);
        return sdf;
    }

    /**
     * Formats the given calendar date using the given format string.
     * @param format the format string
     * @param calendar the calendar to format
     * @return a string representation of the given date
     */
    @ObjectiveCName("formatDate:calendar:")
    public static String formatDate(String format, Calendar calendar) {
        if (calendar == null) {
            return null;
        }

        final SimpleDateFormat sdf = createSimpleDateFormatter(format);
        return formatDate(sdf, calendar);
    }

    /**
     * Formats the given calendar date using the given formatter.
     * @param sdf the date format
     * @param calendar the calendar to format
     * @return a string representation of the given date.
     */
    public static String formatDate(SimpleDateFormat sdf, Calendar calendar) {
        if (calendar == null) {
            return null;
        }

        final Date date = calendar.getTime();
        sdf.setTimeZone(calendar.getTimeZone());
        return sdf.format(date);
    }
}
