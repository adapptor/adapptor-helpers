package au.com.adapptor.helpers.universal;

import com.google.common.base.Joiner;
import com.google.j2objc.annotations.ObjectiveCName;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Contains time- and date-related helper methods.
 */
@SuppressWarnings("unused")
public class TimeHelpers {
    /** One second in milliseconds. */
    public static final long kOneSecondInMillis = 1000;

    /** One minute in milliseconds. */
    public static final long kOneMinuteInMillis = kOneSecondInMillis * 60;

    /** One hour in milliseconds. */
    public static final long kOneHourInMillis = kOneMinuteInMillis * 60;

    /** One day in milliseconds. */
    public static final long kOneDayInMillis = kOneHourInMillis * 24;

    /** Perth's time zone. */
    private static final TimeZone kTimeZone = TimeZone.getTimeZone("Australia/Perth");

    /** English/Australia locale, which can be used in string operations, etc. */
    private static final Locale kLocale = new Locale("en", "AU");

    /** ISO date format, returned by several APIs. */
    private static final Pattern kDatePattern = Pattern.compile(
        "^(\\d{4})-?(\\d{2})-?(\\d{2})T(\\d{2}):?(\\d{2}):?(\\d{2})(\\.\\d+)?(Z|[+-]\\d{2}(:?\\d{2})?)?");

    /**
     * Returns a {@link TimeZone} from the specified UTC offset.
     * @param utcOffset the UTC offset, in hours
     * @return the corresponding <code>TimeZone</code>
     */
    public static TimeZone timeZoneFromUtcOffset(float utcOffset) {
        String tz = String.format(getPerthLocale(), "GMT%s%02d:%02d", utcOffset < 0 ? "-" : "+",
            (int) utcOffset, (int) ((utcOffset % 1) * 60));

        return TimeZone.getTimeZone(tz);
    }

    /**
     * Returns Perth's timezone (UTC+8).
     */
    public static TimeZone getPerthTimeZone() {
        return kTimeZone;
    }

    /**
     * Returns a Locale suitable for English Australia.
     */
    public static Locale getPerthLocale() {
        return kLocale;
    }

    /**
     * Return a calendar representing the current date/time in Perth's timezone.
     */
    public static Calendar getCurrentPerthCalendar() {
        return Calendar.getInstance(getPerthTimeZone(), getPerthLocale());
    }

    /**
     * @return a calendar representing the current date/time at a timezone (using Australian locale)
     * @param timeZone the timezone to get the date/time for
     */
    public static Calendar getCurrentCalendarFromTimezone(TimeZone timeZone) {
        return Calendar.getInstance(timeZone, getPerthLocale());
    }

    /**
     * Return a calendar representing the given day/month/year in Perth's timezone.
     * @param year year
     * @param month month of the year, 1-indexed
     * @param day day of the month, 1-indexed
     * @return the specified date in Perth's timezone
     */
    @ObjectiveCName("getPerthCalendar:month:day:")
    public static Calendar getPerthCalendar(int year, int month, int day) {
        final Calendar calendar = getCurrentPerthCalendar();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month - 1);
        calendar.set(Calendar.DATE, day);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.HOUR, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar;
    }

    /**
     * Converts a {@link String} to a {@link Calendar} using the specified {@link TimeZone}.
     * If the timezone is null, it is extracted from the string.
     * @param string the string representation of the date
     * @param tz the timezone
     * @return the new <code>Calendar</code> object
     */
    @ObjectiveCName("dateFromString:timezone:")
    public static Calendar dateFromString(String string, TimeZone tz) {
        if (string == null) {
            return null;
        }

        Matcher m = kDatePattern.matcher(string);
        Calendar date = null;

        if (m.matches()) {
            if (tz == null) {
                tz = TimeZone.getTimeZone("GMT" + m.group(8));
            }

            try {
                date = Calendar.getInstance(tz);

                int year      = Integer.parseInt(m.group(1));
                int month     = Integer.parseInt(m.group(2));
                int day       = Integer.parseInt(m.group(3));
                int hourOfDay = Integer.parseInt(m.group(4));
                int minute    = Integer.parseInt(m.group(5));
                int second    = Integer.parseInt(m.group(6));

                date.set(year, month - 1, day, hourOfDay, minute, second);
                date.set(Calendar.MILLISECOND, 0);
            } catch (NumberFormatException ex) {
                date = null;
            }
        }

        return date;
    }

    /**
     * Returns whether or not {@code time} is in the future.
     * @param time the time to test
     * @return true if {@code time} is in the future; false if it is in the past
     */
    @ObjectiveCName("timeIsInTheFuture:")
    public static boolean timeIsInTheFuture(Calendar time) {
        return timeIsInTheFuture(time, getCurrentPerthCalendar());
    }

    /**
     * Returns whether or not {@code time} is in the future.
     * @param time the time to test
     * @param now the current time
     * @return true if {@code time} is in the future; false if it is in the past
     */
    public static boolean timeIsInTheFuture(Calendar time, Calendar now) {
        return time.getTimeInMillis() > now.getTimeInMillis();
    }

    /**
     * Returns whether or not {@code time} is in the past.
     * @param time the time to test
     * @return true if {@code time} is in the past; false if it is in the future
     */
    public static boolean timeIsInThePast(Calendar time) {
        return timeIsInThePast(time, getCurrentPerthCalendar());
    }

    /**
     * Returns whether or not {@code time} is in the past.
     * @param time the time to test
     * @param now the current time
     * @return true if {@code time} is in the past; false if it is in the future
     */
    public static boolean timeIsInThePast(Calendar time, Calendar now) {
        return time.getTimeInMillis() < now.getTimeInMillis();
    }

    /**
     * Returns whether or not {@code time} is within the next {@code minutes} minutes.
     * @param time the time to test
     * @param minutes the number of minutes
     * @return true if {@code time} is within the next {@code minutes} minutes; false if it is not
     */
    public static boolean timeIsWithinTheNextMinutes(Calendar time, int minutes) {
        return timeIsWithinTheNextHours(time, getCurrentPerthCalendar(), minutes);
    }

    /**
     * Returns whether or not {@code time} is within the next {@code minutes} minutes.
     * @param time the time to test
     * @param now the current time
     * @param minutes the number of minutes
     * @return true if {@code time} is within the next {@code minutes} minutes; false if it is not
     */
    public static boolean timeIsWithinTheNextMinutes(Calendar time, Calendar now, int minutes) {
        long span = time.getTimeInMillis() - now.getTimeInMillis();
        return 0 < span && span < minutes * kOneMinuteInMillis;
    }

    /**
     * Returns whether or not {@code time} is within the previous {@code minutes} minutes.
     * @param time the time to test
     * @param minutes the number of minutes
     * @return true if {@code time} is within the previous {@code minutes} minutes; false if it is not
     */
    public static boolean timeIsWithinThePreviousMinutes(Calendar time, int minutes) {
        return timeIsWithinThePreviousMinutes(time, getCurrentPerthCalendar(), minutes);
    }

    /**
     * Returns whether or not {@code time} is within the previous {@code minutes} minutes.
     * @param time the time to test
     * @param now the current time
     * @param minutes the number of minutes
     * @return true if {@code time} is within the previous {@code minutes} minutes; false if it is not
     */
    public static boolean timeIsWithinThePreviousMinutes(Calendar time, Calendar now, int minutes) {
        long span = now.getTimeInMillis() - time.getTimeInMillis();
        return 0 < span && span < minutes * kOneMinuteInMillis;
    }

    /**
     * Returns whether or not {@code time} is within the next {@code hours} hours.
     * @param time the time to test
     * @param hours the number of hours
     * @return true if {@code time} is within the next {@code hours} hours; false if it is not
     */
    public static boolean timeIsWithinTheNextHours(Calendar time, int hours) {
        return timeIsWithinTheNextHours(time, getCurrentPerthCalendar(), hours);
    }

    /**
     * Returns whether or not {@code time} is within the next {@code hours} hours.
     * @param time the time to test
     * @param now the current time
     * @param hours the number of hours
     * @return true if {@code time} is within the next {@code hours} hours; false if it is not
     */
    public static boolean timeIsWithinTheNextHours(Calendar time, Calendar now, int hours) {
        long span = time.getTimeInMillis() - now.getTimeInMillis();
        return 0 < span && span < hours * kOneHourInMillis;
    }

    /**
     * Returns whether or not {@code time} is within the previous {@code hours} hours.
     * @param time the time to test
     * @param hours the number of hours
     * @return true if {@code time} is within the previous {@code hours} hours; false if it is not
     */
    public static boolean timeIsWithinThePreviousHours(Calendar time, int hours) {
        return timeIsWithinThePreviousHours(time, getCurrentPerthCalendar(), hours);
    }

    /**
     * Returns whether or not {@code time} is within the previous {@code hours} hours.
     * @param time the time to test
     * @param now the current time
     * @param hours the number of hours
     * @return true if {@code time} is within the previous {@code hours} hours; false if it is not
     */
    public static boolean timeIsWithinThePreviousHours(Calendar time, Calendar now, int hours) {
        long span = now.getTimeInMillis() - time.getTimeInMillis();
        return 0 < span && span < hours * kOneHourInMillis;
    }

    /**
     * Returns whether or not {@code time} is within the next {@code days} days.
     * @param time the time to test
     * @param days the number of days
     * @return true if {@code time} is within the next {@code days} days; false if it is not
     */
    public static boolean timeIsWithinTheNextDays(Calendar time, int days) {
        return timeIsWithinTheNextDays(time, getCurrentPerthCalendar(), days);
    }

    /**
     * Returns whether or not {@code time} is within the next {@code days} days.
     * @param time the time to test
     * @param now the current time
     * @param days the number of days
     * @return true if {@code time} is within the next {@code days} days; false if it is not
     */
    public static boolean timeIsWithinTheNextDays(Calendar time, Calendar now, int days) {
        long span = time.getTimeInMillis() - now.getTimeInMillis();
        return 0 < span && span < days * kOneDayInMillis;
    }

    /**
     * Returns whether or not {@code time} is within the previous {@code days} days.
     * @param time the time to test
     * @param days the number of days
     * @return true if {@code time} is within the previous {@code days} days; false if it is not
     */
    public static boolean timeIsWithinThePreviousDays(Calendar time, int days) {
        return timeIsWithinThePreviousDays(time, getCurrentPerthCalendar(), days);
    }

    /**
     * Returns whether or not {@code time} is within the previous {@code days} days.
     * @param time the time to test
     * @param now the current time
     * @param days the number of days
     * @return true if {@code time} is within the previous {@code days} days; false if it is not
     */
    public static boolean timeIsWithinThePreviousDays(Calendar time, Calendar now, int days) {
        long span = now.getTimeInMillis() - time.getTimeInMillis();
        return 0 < span && span < days * kOneDayInMillis;
    }

    /**
     * Returns the number of minutes between the current time and {@code time} or
     * 0 if {@code time} is in the past. If there is less than one minute until the
     * given time, it will return 1, between one and two minutes will return 2 etc.
     * @param time the time to test
     * @return the number of minutes between the current time and {@code time}
     */
    public static long minutesUntilTime(Calendar time) {
        return minutesUntilTime(time, getCurrentPerthCalendar());
    }

    /**
     * Returns the number of minutes between the times {@code now} and {@code time} or 0 if
     * {@code time} is in the past. If there is less than one minute until the given time,
     * it will return 1, between one and two minutes will return 2 etc.
     * @param time the time to test
     * @param now the current time
     * @return the number of minutes between the current time and {@code time}
     */
    public static long minutesUntilTime(Calendar time, Calendar now) {
        if (timeIsInTheFuture(time, now)) {
            long span = time.getTimeInMillis() - now.getTimeInMillis();
            return (span / kOneMinuteInMillis) + 1;
        } else {
            return 0;
        }
    }

    /**
     * Returns the number of seconds between the current time and {@code time} or
     * 0 if {@code time} is in the past.
     * @param time the time to test
     * @return the number of seconds between the current time and {@code time}
     */
    public static long secondsUntilTime(Calendar time) {
        return secondsUntilTime(time, getCurrentPerthCalendar());
    }

    /**
     * Returns the number of seconds between the times {@code now} and {@code time} or 0 if
     * {@code time} is in the past.
     * @param time the time to test
     * @param now the current time
     * @return the number of seconds between the current time and {@code time}
     */
    public static long secondsUntilTime(Calendar time, Calendar now) {
        if (timeIsInTheFuture(time, now)) {
            long span = time.getTimeInMillis() - now.getTimeInMillis();
            return (span / kOneSecondInMillis);
        } else {
            return 0;
        }
    }

    /**
     * Returns a string representation of a time span, with the components being comma separated.
     * To avoid negative numbers, {@code earlier} should be earlier than {@code later}.
     * Equivalent to calling {@link #commaSeparatedTimeSpan(Calendar, Calendar)}
     * passing {@link #getCurrentPerthCalendar()} as the second argument.
     * @param earlier the earlier time forming the time span
     * @return the time span between {@code time1} and {@code time2}
     */
    public static String commaSeparatedTimeSpan(Calendar earlier) {
        return commaSeparatedTimeSpan(earlier, getCurrentPerthCalendar());
    }

    /**
     * Returns a string representation of a time span, with the components being comma separated.
     * Will return null if {@code earlier} is later than {@code later}.
     * @param earlier the earlier time forming the time span
     * @param later the later time forming the time span
     * @return the time span between {@code earlier} and {@code later}
     */
    public static String commaSeparatedTimeSpan(Calendar earlier, Calendar later) {
        return commaSeparatedTimeSpan(later.getTimeInMillis() - earlier.getTimeInMillis());
    }

    /**
     * Returns a string representation of a time span, with the components being comma separated.
     * Will return null if {@code millis} is negative.
     * @param millis the time span in milliseconds
     * @return the time span of {@code millis}
     */
    public static String commaSeparatedTimeSpan(long millis) {
        long diff = millis;

        if (diff < 0) {
            return null;
        }

        final List<String> parts = new ArrayList<>();
        long days = diff / TimeHelpers.kOneDayInMillis;

        if (days > 0) {
            parts.add(days + " days");
        }

        diff %= TimeHelpers.kOneDayInMillis;
        long hours = diff / TimeHelpers.kOneHourInMillis;

        if (hours > 0) {
            parts.add(hours + " hours");
        }

        diff %= TimeHelpers.kOneHourInMillis;
        long minutes = diff / TimeHelpers.kOneMinuteInMillis;

        if (minutes > 0) {
            parts.add(minutes + " minutes");
        }

        return Joiner.on(", ").join(parts);
    }

    /**
     * Creates a 12 hour time string i.e. "2:30PM" from an hour and minutes parameters
     * @param hour the hour, in 24-hour time
     * @param minute the minutes
     * @return a 12-hour time string
     */
    public static String getTwelveHourTimeString(int hour, int minute) {
        String period = "AM";
        if (hour == 0) {
            hour = 12;
        } else if (hour >= 12) {
            if (hour != 12) {
                hour -= 12;
            }
            period = "PM";
        }
        return String.format(Locale.getDefault(), "%d:%02d%s", hour, minute, period);
    }

    /**
     * Returns the difference in calendar days i.e. would return 1 if date1 is the 30th and date2 is
     * the 29th of the same month (regardless of the time)
     * @param date1 the first date to compare
     * @param date2 the second date to compare
     * @return the difference in days between the two dates
     */
    public static int differenceInCalendarDays(Calendar date1, Calendar date2) {
        Calendar dayOne = (Calendar) date1.clone(), dayTwo = (Calendar) date2.clone();

        if (dayOne.get(Calendar.YEAR) == dayTwo.get(Calendar.YEAR)) {
            return dayOne.get(Calendar.DAY_OF_YEAR) - dayTwo.get(Calendar.DAY_OF_YEAR);
        } else {
            int extraDays = 0;
            int dayOneOriginalYearDays = dayOne.get(Calendar.DAY_OF_YEAR);

            if (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
                while (dayOne.get(Calendar.YEAR) > dayTwo.get(Calendar.YEAR)) {
                    dayOne.add(Calendar.YEAR, -1);
                    // getActualMaximum() important for leap years
                    extraDays += dayOne.getActualMaximum(Calendar.DAY_OF_YEAR);
                }
            } else {
                while (dayOne.get(Calendar.YEAR) < dayTwo.get(Calendar.YEAR)) {
                    dayOne.add(Calendar.YEAR, 1);
                    extraDays -= dayOne.getActualMaximum(Calendar.DAY_OF_YEAR);
                }
            }
            return extraDays - dayTwo.get(Calendar.DAY_OF_YEAR) + dayOneOriginalYearDays;
        }
    }

    /**
     * Creates an ISO8601 compliant string from a calendar instance
     * @param dateTime the calendar instance to create the string from
     * @return a ISO8601 string representing the calendar's date/time
     */
    public static String getIso8601StringFromCalendar(Calendar dateTime, TimeZone tz) {
        Date date = dateTime.getTime();
        String formatted = Formatters.createSimpleDateFormatter("yyyy-MM-dd'T'HH:mm:ssZ", tz)
            .format(date);
        return formatted.substring(0, 22) + ":" + formatted.substring(22);
    }

    /**
     * Returns a string of the current Perth time in ISO8601 format
     */
    public static String getIso8601PerthTimeNow() {
        return getIso8601StringFromCalendar(getCurrentPerthCalendar(), getPerthTimeZone());
    }

    /**
     * Get a new <code>Calendar</code> instance from a date and timezone.
     * @param date the date instance to convert
     * @param timeZone the timezone to use when creating the <code>Calendar</code>
     * @return a new <code>Calendar</code> instance
     */
    public static Calendar fromDate(Date date, TimeZone timeZone) {
        Calendar cal = Calendar.getInstance(timeZone);
        cal.setTime(date);
        return cal;
    }

    /**
     * Get a new <code>Calendar</code> instance from a date and in the Perth time-zone.
     * @param date the date instance to convert
     * @return a new <code>Calendar</code> instance
     */
    public static Calendar fromDate(Date date) {
        return fromDate(date, getPerthTimeZone());
    }
}
