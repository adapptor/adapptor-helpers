package au.com.adapptor.helpers.universal;

@SuppressWarnings("unused")
public final class MiscHelpers {
    /**
     * Returns the first non-null object from {@code objs}.
     * @param objs the input objects
     * @param <T> any type
     * @return the first non-null object
     */
    @SafeVarargs
    public static <T> T firstNotNull(T... objs) {
        for (T t : objs) {
            if (t != null) {
                return t;
            }
        }

        return null;
    }

    /**
     * Returns the first non-zero number from {@code nums}.
     * Only works with the standard number types.
     * {@code nums} must not be empty.
     * @param nums the input numbers
     * @param <T> any {@link Number} type
     * @return the first non-zero number
     */
    @SafeVarargs
    public static <T extends Number> T firstNotZero(T... nums) {
        for (T t : nums) {
            if (t instanceof Float || t instanceof Double) {
                if (t.doubleValue() != 0) {
                    return t;
                }
            } else {
                if (t.longValue() != 0) {
                    return t;
                }
            }
        }

        return nums[0];
    }

    /**
     * Returns the minimum of t1 and t2.
     * @param t1 the first object
     * @param t2 the second object
     * @param <T> any <code>Comparable</code> type
     * @return the minimum of the two
     */
    public static <T extends Comparable<T>> T min(T t1, T t2) {
        return t1.compareTo(t2) < 0 ? t1 : t2;
    }

    /**
     * Returns the maximum of t1 and t2.
     * @param t1 the first object
     * @param t2 the second object
     * @param <T> any <code>Comparable</code> type
     * @return the maximum of the two
     */
    public static <T extends Comparable<T>> T max(T t1, T t2) {
        return t1.compareTo(t2) > 0 ? t1 : t2;
    }

    /**
     * Converts a string to title case.
     * @param str the input string
     * @return the string in title case
     */
    public static String toTitleCase(String str) {
        final String delimiters = " '-/(";
        final StringBuilder sb = new StringBuilder(str.length());
        boolean capNext = true;

        for (char c : str.toCharArray()) {
            sb.append(capNext ? Character.toUpperCase(c) : Character.toLowerCase(c));
            capNext = delimiters.indexOf(c) >= 0;
        }

        return sb.toString();
    }

    /**
     * Compares version strings in dotted digit form. Performs minimal syntax checking.
     * @param version1 the first version string to be compared
     * @param version2 the second version string to be compared
     * @return 0 if the version strings are equal;
     *         negative if the first is less than the second;
     *         positive if the first is greater than the second
     */
    public static int compareVersionStrings(String version1, String version2) {
        final String[] v1 = version1.split("\\.");
        final String[] v2 = version2.split("\\.");
        int i = 0;

        while (i < v1.length && i < v2.length && v1[i].equals(v2[i])) {
            i++;
        }

        return Integer.signum(i < v1.length && i < v2.length
            ? Integer.valueOf(v1[i]).compareTo(Integer.valueOf(v2[i]))
            : v1.length - v2.length);
    }
}
