package au.com.adapptor.helpers.universal;

import com.google.j2objc.annotations.ObjectiveCName;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Utility class for helping with credit card validation
 */
public class CreditCardHelpers {

    public enum CreditCardType {
        INVALID, //for invalid credit card numbers
        UNKNOWN, //for valid credit card numbers where we don't know the brand
        AMERICAN_EXPRESS,
        MASTERCARD,
        VISA,
        DINERS_CLUB_INTERNATIONAL,
        JCB
    }

    /**
     * Looks at a credit card number string and determines if it is a valid number and attempts
     * to determine the type of card based off the Issuer identification number (IIN)
     *
     * Currently supports the following cards:
     *  - VISA
     *  - MasterCard
     *  - American Express
     *  - Diners Club International
     *  - JCB
     *
     * @param creditCardNumberString the credit card string to analyse
     * @return the credit card type if valid, or CreditCardType.INVALID id the card is invalid.
     */
    @ObjectiveCName("validateCreditCard:")
    public static CreditCardType validateCreditCard(String creditCardNumberString) {

        Pattern nonNumericalCheck = Pattern.compile("[^0-9]+$");
        Matcher matcher = nonNumericalCheck.matcher(creditCardNumberString);
        if (matcher.find()) {
            return CreditCardType.INVALID;
        }

        //use luhn-checksum to check validity of credit card:
        if (!luhnCheckCreditCardString(creditCardNumberString)) {
            return CreditCardType.INVALID;
        }

        if (creditCardNumberString.length() < 13 || creditCardNumberString.length() > 19) {
            return CreditCardType.INVALID;
        }

        int iin = Integer.parseInt(creditCardNumberString.substring(0, 6));

        if (((iin >= 340000 && iin <= 349999)  ||
            (iin >= 370000 && iin <= 379999)) &&
            creditCardNumberString.length() == 15) {
            return CreditCardType.AMERICAN_EXPRESS;
        }

        if ((iin >= 222100 && iin <= 272099) ||
            (iin >= 500000 && iin <= 599999) &&
            creditCardNumberString.length() == 16) {
            return CreditCardType.MASTERCARD;
        }

        if ((iin >= 400000 && iin <= 499999) &&
            (creditCardNumberString.length() == 13 ||
            creditCardNumberString.length() == 16 ||
            creditCardNumberString.length() == 19)) {
            return CreditCardType.VISA;
        }

        if ((iin >= 300000 && iin <= 305999) ||
            (iin >= 309000 && iin <= 309999) ||
            (iin >= 360000 && iin <= 369999) ||
            (iin >= 380000 && iin <= 399999) &&
            creditCardNumberString.length() == 14) {
            return CreditCardType.DINERS_CLUB_INTERNATIONAL;
        }

        if ((iin >= 350000 && iin <= 359999) &&
            creditCardNumberString.length() == 16) {
            return CreditCardType.JCB;
        }

        return CreditCardType.UNKNOWN;
    }

    /**
     * Uses Luhn Checksum to validate a credit card number
     * see https://en.wikipedia.org/wiki/Luhn_algorithm
     * @param creditCardString the credit card string to check
     * @return true if credit card string passes the Luhn Check, false otherwise
     */
    private static boolean luhnCheckCreditCardString(String creditCardString) {
        boolean odd = false;
        int total = 0;
        for (int i = creditCardString.length() - 1; i >= 0; i--) {
            int digit = Integer.parseInt(creditCardString.substring(i, i + 1));

            if (odd) {
                digit *= 2;
                if (digit > 9) {
                    digit -= 9;
                }
            }
            odd = !odd;
            total += digit;
        }

        return total % 10 == 0;
    }

}
