package au.com.adapptor.helpers.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatEditText;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.google.common.base.Strings;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import au.com.adapptor.helpers.R;

/**
 * An {@link EditText} that performs input similar to an ATM, with a set number of decimal places.
 */
@SuppressWarnings("unused")
public class AtmStyleEditText extends AppCompatEditText {
    /** The class tag. */
    private static final String kTag = "AtmStyleEditText";

    /** The default number of decimal places, if not specified. */
    private static final int kDefaultDecimalPlaces = 2;

    /** The number of decimal places to be displayed. */
    private int mDecimalPlaces = kDefaultDecimalPlaces;

    /** A prefix to be prepended to the text. */
    private String mPrefix;

    /** Indicates that this control is not currently updating itself. */
    private boolean mNotUpdating;

    /** A list of text change watchers. */
    private List<TextWatcher> mListeners;

    /**
     * Constructor.
     * @param context the current context
     */
    public AtmStyleEditText(Context context) {
        super(context);
        init();
    }

    /**
     * Constructor.
     * @param context the current context
     * @param attrs the attribute set
     */
    public AtmStyleEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        getAttributes(context, attrs);
        init();
    }

    /**
     * Constructor.
     * @param context the current context
     * @param attrs the attribute set
     * @param defStyleAttr default style resource
     */
    public AtmStyleEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        getAttributes(context, attrs);
        init();
    }

    /**
     * Common initialisation.
     */
    private void init() {
        setInputType(InputType.TYPE_CLASS_NUMBER);
        mNotUpdating = true;
        setText(mPrefix);
    }

    /**
     * Gets attribute values, such as the number of decimal places to be displayed.
     * @param context the context to use
     * @param attrs the attribute set
     */
    private void getAttributes(Context context, AttributeSet attrs) {
        final TypedArray a = context.getTheme().obtainStyledAttributes(
            attrs, R.styleable.AtmStyleEditText, 0, 0);

        try {
            mDecimalPlaces = a.getInt(R.styleable.AtmStyleEditText_decimalPlaces, kDefaultDecimalPlaces);

            if (mDecimalPlaces < 0) {
                throw new IllegalArgumentException("decimalPlaces must be >= 0.");
            }

            mPrefix = a.getString(R.styleable.AtmStyleEditText_prefix);

            if (mPrefix == null) {
                mPrefix = "";
            }
        } finally {
            a.recycle();
        }
    }

    @Override
    public void addTextChangedListener(TextWatcher watcher) {
        if (mListeners == null) {
            mListeners = new ArrayList<>();
        }

        mListeners.add(watcher);
    }

    @Override
    public void removeTextChangedListener(TextWatcher watcher) {
        if (mListeners != null) {
            int i = mListeners.indexOf(watcher);

            if (i >= 0) {
                mListeners.remove(i);
            }
        }
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        setSelection(length());
    }

    @Override
    protected void onTextChanged(CharSequence text, int start, int lengthBefore, int lengthAfter) {
        if (!mNotUpdating) {
            return;
        }

        String newText = text.toString().replaceFirst(Pattern.quote(mPrefix), "");
        newText = newText.replaceFirst("^0", "").replaceFirst("\\.", "");

        String integral, fractional;

        if (newText.length() <= mDecimalPlaces) {
            integral = "0";
            fractional = Strings.padStart(newText, mDecimalPlaces, '0');
        } else {
            int dp = newText.length() - mDecimalPlaces;
            integral = newText.substring(0, dp);
            fractional = newText.substring(dp);
        }

        newText = String.format("%s%s%s", mPrefix, integral, mDecimalPlaces == 0 ? "" : "." + fractional);

        mNotUpdating = false;

        try {
            /*
             * This is a little bit hacky, but I couldn't think of a better/simpler way of doing it.
             * Listeners are only called for text changes that are performed by this method.
             */
            @SuppressWarnings("JavaReflectionMemberAccess")
            final Field f = TextView.class.getDeclaredField("mListeners");
            f.setAccessible(true);
            f.set(this, mListeners);
            setText(newText);
            f.set(this, null);
        } catch (Exception e) {
            Log.e(kTag, "Error updating control text.", e);
        }

        mNotUpdating = true;
    }

    /**
     * Returns the number of decimal places this control will display.
     * @return the decimal places setting
     */
    public int getDecimalPlaces() {
        return mDecimalPlaces;
    }

    /**
     * Sets the number of decimal places this control will display.
     * @param decimalPlaces the decimal places setting
     */
    public void setDecimalPlaces(int decimalPlaces) {
        mDecimalPlaces = decimalPlaces;
        setText(getText());
    }

    /**
     * Returns the integral part of the currently displayed number.
     * @return the integral part
     */
    public int getIntegralPart() {
        final String text = getText().toString().replaceFirst(Pattern.quote(mPrefix), "");
        return Integer.valueOf(mDecimalPlaces == 0 ? text : text.substring(0, text.indexOf('.')));
    }

    /**
     * Returns the fractional part of the currently displayed number.
     * @return the fractional part
     */
    public int getFractionalPart() {
        final String text = getText().toString().replaceFirst(Pattern.quote(mPrefix), "");
        return mDecimalPlaces == 0 ? 0 : Integer.valueOf(text.substring(text.indexOf('.') + 1));
    }
}
