package au.com.adapptor.helpers.android.helpers;

import android.content.res.Resources;
import android.support.constraint.Group;
import android.view.View;

/**
 * Contains view-related helper methods.
 */
@SuppressWarnings("unused")
public class ViewHelper {
    /**
     * Returns the height of the status bar in pixels, or 0 is not available.
     * @param res the resources to use
     * @return the status bar height in pixels
     */
    public static int getStatusBarHeight(Resources res) {
        int resId = res.getIdentifier("status_bar_height", "dimen", "android");
        return resId > 0 ? res.getDimensionPixelSize(resId) : 0;
    }

    /**
     * Returns the height of the navigation bar in pixels, or 0 if not available.
     * @param res the resources to use
     * @return the navigation bar height in pixels
     */
    public static int getNavigationBarHeight(Resources res) {
        int resId = res.getIdentifier("navigation_bar_height", "dimen", "android");
        return resId > 0 ? res.getDimensionPixelSize(resId) : 0;
    }

    /**
     * Sets the alpha of each view in the group.
     * Hopefully, this will be added to {@link Group} eventually, at which point, this can be removed.
     * @param group the group whose alpha is to be set
     * @param alpha the alpha value to set (0=transparent, 1=opaque)
     */
    public static void setAlpha(Group group, float alpha) {
        final View parent = (View) group.getParent();

        for (int id : group.getReferencedIds()) {
            final View view = parent.findViewById(id);
            view.setAlpha(alpha);
        }
    }
}
