package au.com.adapptor.helpers.android.text;

import android.text.Editable;
import android.text.TextWatcher;

/**
 * An adapter for Android's {@link TextWatcher} class.
 */
public abstract class TextWatcherAdapter implements TextWatcher {
    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}
