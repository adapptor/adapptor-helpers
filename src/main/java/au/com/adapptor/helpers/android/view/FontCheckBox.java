package au.com.adapptor.helpers.android.view;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.util.AttributeSet;

@SuppressWarnings("unused")
public class FontCheckBox extends AppCompatCheckBox implements FontTextViewBase {
    public FontCheckBox(Context context) {
        super(context);
        init(null);
    }

    public FontCheckBox(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FontCheckBox(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }
}
