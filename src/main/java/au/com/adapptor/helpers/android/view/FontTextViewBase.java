package au.com.adapptor.helpers.android.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import au.com.adapptor.helpers.R;

@SuppressLint("NewApi")
@SuppressWarnings("unused")
public interface FontTextViewBase {
    Context getContext();
    boolean isInEditMode();
    void setTypeface(Typeface tf);

    default void init(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextViewBase);
            String fontName = a.getString(R.styleable.FontTextViewBase_textFont);

            if (fontName != null && !isInEditMode()) {
                setFont(fontName);
            }

            a.recycle();
        }
    }

    default void setFont(String fontName) {
        Typeface tf = Fonts.getInstance().getTypeface("fonts/" + fontName);
        setTypeface(tf);
    }
}
