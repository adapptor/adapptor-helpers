package au.com.adapptor.helpers.android.helpers;

import android.annotation.TargetApi;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Converts Serializable objects to and from byte arrays
 */
@TargetApi(19)
public class Serializer {

    /**
     * Converts an object to a byte array
     * @param obj the object to convert
     * @return the byte array
     * @throws IOException thrown if the conversion fails
     */
    public static byte[] serialize(Object obj) throws IOException {
        try(ByteArrayOutputStream b = new ByteArrayOutputStream()){
            try(ObjectOutputStream o = new ObjectOutputStream(b)){
                o.writeObject(obj);
            }
            return b.toByteArray();
        }
    }

    /**
     * Converts a byte array to an object
     * @param bytes the byte array to convert
     * @return the object to convert to
     * @throws IOException thrown if the conversion fails
     * @throws ClassNotFoundException thrown if the conversion fails
     */
    public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
        try(ByteArrayInputStream b = new ByteArrayInputStream(bytes)){
            try(ObjectInputStream o = new ObjectInputStream(b)){
                return o.readObject();
            }
        }
    }
}
