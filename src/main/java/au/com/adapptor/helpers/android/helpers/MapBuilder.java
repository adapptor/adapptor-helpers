package au.com.adapptor.helpers.android.helpers;

import java.util.Map;
import java.util.TreeMap;

/**
 * Helper to build a String/Object map with chaining put calls.
 */
@SuppressWarnings("unused")
public class MapBuilder {
    private Map<String, Object> params = new TreeMap<>();

    public MapBuilder put(String key, Object object) {
        params.put(key, object);
        return this;
    }

    public MapBuilder putIf(String key, Object object, boolean predicate) {
        if (predicate) {
            params.put(key, object);
        }

        return this;
    }

    public Map<String, Object> build() {
        final Map<String, Object> obj = params;
        params = null;
        return obj;
    }
}
