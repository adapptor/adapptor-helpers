package au.com.adapptor.helpers.android.core;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import au.com.adapptor.helpers.android.helpers.JsonHelper;

@SuppressWarnings("unused")
public class Theme {
    private static final String TAG = "Theme";

    private static Theme sInstance;
    private final Map<String, Object> mTheme;

    private static final String kColorRegex = "#?([0-9a-fA-F]{6})";
    private static Pattern sColorPattern;

    public static void init(Context context, String assetPath) {
        try {
            InputStream stream = context.getAssets().open(assetPath);
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, "UTF-8"));
            sInstance = new Theme(reader);
        } catch (IOException ex) {
            Log.e(TAG, "Theme init error", ex);
        }
    }

    public static void initWithString(String json) {
        sInstance = new Theme(json);
    }

    public static Theme instance() {
        return sInstance;
    }

    public Theme(Reader reader) {
        JsonHelper json = new JsonHelper();
        mTheme = json.fromJson(reader);
    }

    public Theme(String themeJSON) {
        JsonHelper json = new JsonHelper();
        mTheme = json.fromJson(themeJSON);
    }

    public boolean bool(String key) {
        Object obj = mTheme.get(key);
        return (obj instanceof Boolean) && (Boolean) obj;
    }

    public String string(String key) {
        Object obj = mTheme.get(key);
        return (obj instanceof String) ? (String) obj : null;
    }

    public int integer(String key) {
        Object obj = mTheme.get(key);
        return (obj instanceof Number) ? ((Number) obj).intValue() : 0;
    }

    public double number(String key) {
        Object obj = mTheme.get(key);
        return (obj instanceof Number) ? ((Number) obj).doubleValue() : 0.0;
    }

    public int color(String key) {
        String colorString = string(key);

        if (colorString == null) {
            return 0;
        }

        if (sColorPattern == null) {
            sColorPattern = Pattern.compile(kColorRegex);
        }

        Matcher match = sColorPattern.matcher(colorString);
        return match.matches() ? Integer.parseInt(match.group(1), 16) | 0xff000000 : 0;
    }

    public double timeInterval(String key) {
        Object obj = mTheme.get(key);
        return (obj instanceof Number) ? ((Number) obj).doubleValue() : 0.0;
    }
}
