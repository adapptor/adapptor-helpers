package au.com.adapptor.helpers.android.view;

import android.content.Context;
import android.support.v7.widget.AppCompatAutoCompleteTextView;
import android.util.AttributeSet;

@SuppressWarnings("unused")
public class FontAutoCompleteTextView extends AppCompatAutoCompleteTextView implements FontTextViewBase {
    public FontAutoCompleteTextView(Context context) {
        super(context);
        init(null);
    }

    public FontAutoCompleteTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FontAutoCompleteTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }
}
