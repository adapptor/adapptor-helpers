package au.com.adapptor.helpers.android.view;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.View;

import au.com.adapptor.helpers.R;

/**
 * Displays a series of dots, with one being highlighted as the selected dot.
 */
@SuppressWarnings("unused")
public class DotPager extends View {
    protected int mPageCount;
    protected int mCurrentPage;

    protected float mSpacing;
    protected float mCircleSize;

    private final Paint mPaint = new Paint();

    protected int mDotColor;
    protected int mCurrentDotColor;

    protected RectF mTempRectF = new RectF();

    public DotPager(Context context) {
        super(context);
        init(context, null);
    }

    public DotPager(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public DotPager(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs);
    }

    @SuppressWarnings("deprecation")
    private void init(Context context, AttributeSet attrs) {
        final Resources res = getResources();

        mSpacing = res.getDimensionPixelSize(R.dimen.dot_pager_spacing);
        mCircleSize = res.getDimensionPixelSize(R.dimen.dot_pager_circle_size);

        mPaint.setAntiAlias(true);

        setUpDotColours(context, attrs, res);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        if (mPageCount == 0) {
            return;
        }

        float totalWidth = mPageCount * (mCircleSize + mSpacing) - mSpacing;
        float totalLeft = (getWidth() - totalWidth) / 2;

        mTempRectF.top = getPaddingTop();
        mTempRectF.bottom = getHeight() - getPaddingBottom();

        float tabWidth = mCircleSize;

        for (int i = 0; i < mPageCount; ++i) {
            mTempRectF.left = totalLeft + (i * (tabWidth + mSpacing));
            mTempRectF.right = mTempRectF.left + tabWidth;

            float cx = mTempRectF.centerX();
            float cy = mTempRectF.centerY();

            mPaint.setColor(i == mCurrentPage ? mCurrentDotColor : mDotColor);
            canvas.drawCircle(cx, cy, mCircleSize * 0.5f, mPaint);
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        setMeasuredDimension(
            View.resolveSize(
                (int) (mPageCount * (mCircleSize + mSpacing) - mSpacing) + getPaddingLeft() + getPaddingRight(),
                widthMeasureSpec),
            View.resolveSize(
                (int) mCircleSize + getPaddingTop() + getPaddingBottom(),
                heightMeasureSpec));
    }

    /**
     * Sets the current page.
     * @param currentPage the page to set
     */
    public void setCurrentPage(int currentPage) {
        mCurrentPage = currentPage;
        invalidate();
    }

    /**
     * Sets the page count.
     * @param count the page count
     */
    public void setPageCount(int count) {
        mPageCount = count;
        invalidate();
    }

    /**
     * @return the dot colour (unselected) as an int
     */
    public int getDotColor() {
        return mDotColor;
    }

    /**
     * Sets the unselected dot colour
     * @param dotColor colour to set to
     */
    public void setDotColor(int dotColor) {
        mDotColor = dotColor;
    }

    /**
     * @return the dot colour (selected) as an int
     */
    public int getCurrentDotColor() {
        return mCurrentDotColor;
    }

    /**
     * Sets the selected dot colour
     * @param currentDotColor colour to set to
     */
    public void setCurrentDotColor(int currentDotColor) {
        mCurrentDotColor = currentDotColor;
    }

    /**
     * Assigns custom colors to the dots if they are defined in the layout xml
     * @param context
     * @param attrs
     */
    @SuppressWarnings("deprecation")
    private void setUpDotColours(Context context, AttributeSet attrs, Resources res) {

        if (attrs == null) {
            mDotColor = res.getColor(R.color.dot_pager_dot);
            mCurrentDotColor = res.getColor(R.color.dot_pager_dot_selected);
            return;
        }

        final TypedArray a = context.getTheme().obtainStyledAttributes(
            attrs,
            R.styleable.DotPager,
            0, 0);

        try {
            mDotColor = a.getColor(R.styleable.DotPager_dot_colour_unselected, res.getColor(R.color.dot_pager_dot));
            mCurrentDotColor = a.getColor(R.styleable.DotPager_dot_colour_selected, res.getColor(R.color.dot_pager_dot_selected));
        } finally {
            a.recycle();
        }
    }
}
