package au.com.adapptor.helpers.android.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import au.com.adapptor.helpers.R;
import au.com.adapptor.helpers.android.helpers.LogHelper;
import io.reactivex.functions.Action;

/**
 * A custom {@link ViewPager} that displays a single view and performs an action when the user slides to the left.
 */
public class ActionSlider extends ViewPager {
    /**
     * Defines the possible slide directions.
     */
    public enum SlideDirection {
        LEFT,
        RIGHT
    }

    /** The class tag. */
    private static final String TAG = "ActionSlider";

    /** The view to be displayed. */
    private View mView;

    /** The action to run when the user slides. */
    private Action mAction;

    /** Whether or not the slider is enabled. */
    private boolean mEnabled;

    /** The direction the user has to slide in order to run the action. */
    private SlideDirection mSlideDirection;

    /**
     * Constructor.
     * @param context the context
     * @param layoutResId the resource ID of the layout to use
     */
    public ActionSlider(Context context, int layoutResId) {
        this(context, layoutResId, SlideDirection.LEFT);
    }

    /**
     * Constructor.
     * @param context the context
     * @param layoutResId the resource ID of the layout to use
     * @param slideDirection the slide direction
     */
    public ActionSlider(Context context, int layoutResId, SlideDirection slideDirection) {
        super(context);
        mSlideDirection = slideDirection;
        init(context, layoutResId);
    }

    /**
     * Constructor.
     * @param context the context
     * @param attrs the creation attributes
     */
    public ActionSlider(Context context, AttributeSet attrs) {
        super(context, attrs);

        final TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.ActionSlider);
        int layoutResId;

        try {
            layoutResId = a.getResourceId(R.styleable.ActionSlider_layout, 0);

            mEnabled = a.getBoolean(R.styleable.ActionSlider_android_enabled, true);

            mSlideDirection = SlideDirection.values()[a.getInt(
                R.styleable.ActionSlider_slide_direction, SlideDirection.LEFT.ordinal())];
        } finally {
            a.recycle();
        }

        init(context, layoutResId);
    }

    /**
     * Initialises the view pager.
     * @param context the context to use
     * @param layoutResId the resource ID of the layout to use
     */
    private void init(Context context, int layoutResId) {
        mView = View.inflate(context, layoutResId, null);

        setSaveEnabled(false);

        setAdapter(new PagerAdapter() {
            @Override
            public int getCount() {
                return 2;
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                if (position == mSlideDirection.ordinal()) {
                    container.addView(mView);
                    return mView;
                } else {
                    final View v = new View(context);
                    container.addView(v);
                    return v;
                }
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }
        });

        reset();

        addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (position == (mSlideDirection == SlideDirection.LEFT ? 1 : 0)) {
                    try {
                        mAction.run();
                    } catch (Exception e) {
                        LogHelper.e(TAG, "Action threw an exception.", e);
                    }
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    @Override
    @SuppressLint("ClickableViewAccessibility")
    public boolean onTouchEvent(MotionEvent ev) {
        return !mEnabled || super.onTouchEvent(ev);
    }

    /**
     * Gets the view used to display the user prompt.
     * @return the user prompt view
     */
    public View getView() {
        return mView;
    }

    /**
     * Sets the action to be run when the user slides.
     * @param action the action to run
     */
    public void setAction(Action action) {
        mAction = action;
    }

    /**
     * Gets whether or not the slider is enabled.
     * @return whether or not the control is enabled
     */
    @Override
    public boolean isEnabled() {
        return mEnabled;
    }

    /**
     * Sets whether or not the slider is enabled. Note that this does not affect the UI;
     * if a client wants a different UI when the slider is disabled, it must implement it itself.
     * @param enabled whether or not the control is enabled
     */
    @Override
    public void setEnabled(boolean enabled) {
        mEnabled = enabled;
    }

    /**
     * Returns the slider to its initial position (with the view shown).
     */
    public void reset() {
        setCurrentItem(mSlideDirection == SlideDirection.LEFT ? 0 : 1);
    }
}
