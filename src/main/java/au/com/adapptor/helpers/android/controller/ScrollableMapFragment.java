package au.com.adapptor.helpers.android.controller;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.widget.NestedScrollView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;

import com.google.android.gms.maps.SupportMapFragment;

/**
 * A {@link SupportMapFragment} that can be used within a {@link ScrollView} or a {@link NestedScrollView}.
 */
@SuppressWarnings("unused")
public class ScrollableMapFragment extends SupportMapFragment {
    /** The {@link ScrollView} that is hosting this map. */
    private ScrollView mScrollView;

    /** The {@link NestedScrollView} that is hosting this map. */
    private NestedScrollView mNestedScrollView;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle savedInstance) {
        final View view = super.onCreateView(layoutInflater, viewGroup, savedInstance);
        final Glass glass = new Glass(getActivity());

        if (view instanceof ViewGroup) {
            ((ViewGroup) view).addView(glass,
                new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));
        }

        return view;
    }

    /**
     * Sets the {@link ScrollView} that is hosting this map.
     * @param scrollView the hosting {@link ScrollView}
     */
    public void setScrollView(ScrollView scrollView) {
        mScrollView = scrollView;
    }

    /**
     * Sets the {@link NestedScrollView} that is hosting this map.
     * @param scrollView the hosting {@link NestedScrollView}
     */
    public void setScrollView(NestedScrollView scrollView) {
        mNestedScrollView = scrollView;
    }

    /**
     * A transparent view that will sit above the map.
     */
    public class Glass extends View {
        /**
         * Creates a transparent view that will sit above the map.
         * @param context the current context
         */
        public Glass(Context context) {
            super(context);
        }

        @Override
        public boolean dispatchTouchEvent(MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    requestDisallowInterceptTouchEvent(true);
                    break;

                case MotionEvent.ACTION_UP:
                    requestDisallowInterceptTouchEvent(false);
                    break;
            }

            return super.dispatchTouchEvent(event);
        }
    }

    /**
     * Sets {@code requestDisallowInterceptTouchEvent} on the hosting scroll view.
     */
    private void requestDisallowInterceptTouchEvent(boolean disallowIntercept) {
        if (mScrollView != null) {
            mScrollView.requestDisallowInterceptTouchEvent(disallowIntercept);
        }

        if (mNestedScrollView != null) {
            mNestedScrollView.requestDisallowInterceptTouchEvent(disallowIntercept);
        }
    }
}
