package au.com.adapptor.helpers.android.helpers;

import android.util.Log;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import au.com.adapptor.helpers.universal.TimeHelpers;

/**
 * Helper for logging to a file and the standard android log
 */
public class LogHelper {

    /** Class tag */
    private static final String TAG = "LogHelper";

    /** Index into this array using integer constants in android.util.Log */
    private static final String[] sVerbosities = new String[] { "", "", "V", "D", "I", "W", "E", "A" };

    /** Date format pre-pended to each log line */
    private static final SimpleDateFormat sDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", TimeHelpers.getPerthLocale());

    /** Date format appended to log file if it is renamed due to being too large */
    private static final SimpleDateFormat sDateFormatFile = new SimpleDateFormat("yyyyMMddHHmmss", TimeHelpers.getPerthLocale());

    /** Maximum size of the log file, in bytes */
    private static long kMaxLogFileSize = 5 * 1024 * 1024;

    /** Path to current log file */
    private static String sLogFilePath = null;

    /**
     * Current log file verbosity, matching integer constants in android.util.Log.
     * Messages with lower verbosity than this will not be written to the file
     */
    private static int sLogFileVerbosity = 0;

    /**
     * Sets a log file for messages to be written to
     * @param path the path to the file. Please ensure it it is writeable if using external storage.
     * @param verbosity minimum log level to output to file. Corresponds to constants in android.util.Log
     */
    public static void setLogFile(String path, int verbosity) {
        sLogFilePath = path;
        sLogFileVerbosity = verbosity;
        File logFile = new File(path);

        // Rename exiting file it is getting too large
        if (logFile.exists() && logFile.length() > kMaxLogFileSize) {
            int lastDot = path.lastIndexOf('.');
            String pathNew = path;
            String extension = null;
            if (lastDot > 0 && lastDot > path.lastIndexOf(File.pathSeparatorChar)) {
                pathNew = path.substring(0, lastDot);
                extension = path.substring(lastDot + 1);
            }
            pathNew += "-" + sDateFormatFile.format(new Date());
            if (extension != null) {
                pathNew += "." + extension;
            }
            logFile.renameTo(new File(pathNew));
        }
    }

    /**
     * Private constructor
     */
    private LogHelper() {
    }

    /**
     * Logs a message to the current file, if one has been set
     * @param verbosity verbosity level
     * @param tag class tag
     * @param msg optional message
     * @param tr optional throwable
     */
    public static void logToFile(int verbosity, String tag, String msg, Throwable tr) {
        if (sLogFilePath == null) {
            return;
        }

        File logFile = new File(sLogFilePath);
        if (!logFile.exists())  {
            try  {
                Log.e(TAG, "logToFile: creating: " + sLogFilePath);
                logFile.createNewFile();
            } catch (IOException e) {
                Log.e(TAG, "logToFile: failed to create file: " + sLogFilePath);
            }
        }

        final String msgLine = String.format(TimeHelpers.getPerthLocale(), "%s %s/%s: %s",
            sDateFormat.format(new Date()),
            sVerbosities[Math.max(Log.VERBOSE, Math.min(Log.ASSERT, verbosity))],
            tag,
            msg == null ? "" : msg);

        try {
            BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
            buf.write(msgLine);
            buf.newLine();
            if (tr != null) {
                buf.write(Log.getStackTraceString(tr));
                buf.newLine();
            }
            buf.flush();
            buf.close();
        } catch (IOException e) {
            Log.e(TAG, "logToFile: failed to log msg: " + msgLine, e);
        }
    }

    /**
     * Send a verbose log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int v(String tag, String msg) {
        onMessage(Log.VERBOSE, tag, msg, null);
        return Log.v(tag, msg);
    }

    /**
     * Send a VERBOSE log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static int v(String tag, String msg, Throwable tr) {
        onMessage(Log.VERBOSE, tag, msg, tr);
        return Log.v(tag, msg, tr);
    }

    /**
     * Send a DEBUG log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int d(String tag, String msg) {
        onMessage(Log.DEBUG, tag, msg, null);
        return Log.d(tag, msg);
    }

    /**
     * Send a DEBUG log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static int d(String tag, String msg, Throwable tr) {
        onMessage(Log.DEBUG, tag, msg, tr);
        return Log.d(tag, msg, tr);
    }

    /**
     * Send an INFO log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int i(String tag, String msg) {
        onMessage(Log.INFO, tag, msg, null);
        return Log.i(tag, msg);
    }

    /**
     * Send a INFO log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static int i(String tag, String msg, Throwable tr) {
        onMessage(Log.INFO, tag, msg, tr);
        return Log.i(tag, msg, tr);
    }

    /**
     * Send a WARN log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int w(String tag, String msg) {
        onMessage(Log.WARN, tag, msg, null);
        return Log.w(tag, msg);
    }

    /**
     * Send a WARN log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static int w(String tag, String msg, Throwable tr) {
        onMessage(Log.WARN, tag, msg, tr);
        return Log.w(tag, msg, tr);
    }

    /**
     * Send a WARN log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param tr An exception to log
     */
    public static int w(String tag, Throwable tr) {
        onMessage(Log.WARN, tag, null, tr);
        return Log.w(tag, tr);
    }

    /**
     * Send an ERROR log message.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     */
    public static int e(String tag, String msg) {
        onMessage(Log.ERROR, tag, msg, null);
        return Log.e(tag, msg);
    }

    /**
     * Send a ERROR log message and log the exception.
     * @param tag Used to identify the source of a log message.  It usually identifies
     *        the class or activity where the log call occurs.
     * @param msg The message you would like logged.
     * @param tr An exception to log
     */
    public static int e(String tag, String msg, Throwable tr) {
        onMessage(Log.ERROR, tag, msg, tr);
        return Log.e(tag, msg, tr);
    }

    /**
     * Helper for logging to file one has been specified and verbosity >= sLogFileVerbosity
     * @param verbosity verbosity of message
     * @param tag class tag
     * @param msg optional message
     * @param tr optional throwable
     */
    private static void onMessage(int verbosity, String tag, String msg, Throwable tr) {
        if (verbosity >= sLogFileVerbosity) {
            logToFile(verbosity, tag, msg, tr);
        }
    }

}
