package au.com.adapptor.helpers.android.helpers;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.io.Reader;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;

import au.com.adapptor.helpers.universal.JsonHelpers;
import okhttp3.ResponseBody;
import retrofit2.Response;

/**
 * A wrapper for Gson functionality, to serialize/deserialize between JSON and
 * Map<String, Object>
 */
@SuppressWarnings("unused")
public class JsonHelper {
    private final Gson mGson;
    private Type mJsonMapType;

    public JsonHelper() {
        mGson = new GsonBuilder().create();
    }

    public Gson getGson() {
        return mGson;
    }

    // Helpers for simple JSON responses
    public Map<String, Object> fromJson(String json) {
        return mGson.fromJson(json, getJsonMapType());
    }

    public Map<String, Object> fromJson(Reader json) {
        return mGson.fromJson(json, getJsonMapType());
    }

    public String toJson(Map<String, Object> obj) {
        return mGson.toJson(obj, getJsonMapType());
    }

    public List<Object> fromJsonArray(Reader json) {
        Type jsonArrayType = new TypeToken<List<Object>>(){}.getType();
        return mGson.fromJson(json, jsonArrayType);
    }

    public Type getJsonMapType() {
        if (mJsonMapType == null) {
            mJsonMapType = new TypeToken<Map<String, Object>>(){}.getType();
        }

        return mJsonMapType;
    }

    public static Map<String, Object> getMap(Map<String, Object> json, String key) {
        //noinspection unchecked
        return (Map<String, Object>) json.get(key);
    }

    public static List<Map<String, Object>> getList(Map<String, Object> json, String key) {
        //noinspection unchecked
        return (List<Map<String, Object>>) json.get(key);
    }

    public static Map<String, Object> getJsonFromResponse(Response<ResponseBody> r) {
        return JsonHelpers.mapFromJsonObject(r.body().byteStream());
    }
}
