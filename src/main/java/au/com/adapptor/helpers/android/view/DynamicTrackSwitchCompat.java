package au.com.adapptor.helpers.android.view;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.SwitchCompat;
import android.util.AttributeSet;

import au.com.adapptor.helpers.R;

/**
 * A switch which has a different background when checked.
 */
public class DynamicTrackSwitchCompat extends SwitchCompat {
    private Drawable mBackgroundChecked;
    private Drawable mBackgroundUnchecked;

    public DynamicTrackSwitchCompat(Context context) {
        super(context);
    }

    public DynamicTrackSwitchCompat(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpResources(context, attrs);
    }

    public DynamicTrackSwitchCompat(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setUpResources(context, attrs);
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        setTrackDrawable(isChecked() ? mBackgroundChecked : mBackgroundUnchecked);
    }

    private void setUpResources(Context context, AttributeSet attrs) {
        final TypedArray a = context.getTheme().obtainStyledAttributes(
            attrs,
            R.styleable.DynamicTrackSwitchCompat,
            0, 0);

        try {
            mBackgroundChecked = a.getDrawable(R.styleable.DynamicTrackSwitchCompat_checkedTrack);
            mBackgroundUnchecked = a.getDrawable(R.styleable.DynamicTrackSwitchCompat_uncheckedTrack);
        } finally {
            a.recycle();
        }
    }
}
