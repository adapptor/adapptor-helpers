package au.com.adapptor.helpers.android.view;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.drawable.Drawable;

/**
 * A drawable that renders text using a given {@link Paint}.
 */
@SuppressWarnings("unused")
public class TextDrawable extends Drawable {
    /** The text to be drawn. */
    private String mText;

    /** The paint to be used. */
    private Paint mPaint;

    /**
     * Constructor.
     * @param text the text to be drawn
     * @param paint the paint to be used
     */
    public TextDrawable(String text, Paint paint) {
        mText = text;
        mPaint = paint;
    }

    /**
     * Returns the text to be drawn
     * @return the text to be drawn
     */
    public String getText() {
        return mText;
    }

    /**
     * Sets the text to be drawn
     * @param text the text to be drawn
     */
    public void setText(String text) {
        mText = text;
    }

    /**
     * Returns the paint to be used
     * @return the paint to be used
     */
    public Paint getPaint() {
        return mPaint;
    }

    /**
     * Sets the paint to be used
     * @param paint the paint to be used
     */
    public void setPaint(Paint paint) {
        mPaint = paint;
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawText(mText, 0, 0, mPaint);
    }

    @Override
    public void setAlpha(int alpha) {
        mPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mPaint.setColorFilter(cf);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}
