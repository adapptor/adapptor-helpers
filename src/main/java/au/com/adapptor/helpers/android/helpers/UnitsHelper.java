package au.com.adapptor.helpers.android.helpers;

import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Contains helper methods for converting between various units.
 */
@SuppressWarnings("unused")
public final class UnitsHelper {
    /**
     * Converts device independent pixels to pixels.
     * @param dip the number of DIPs
     * @param res the resources to use
     * @return the equivalent value in pixels
     */
    public static float dipToPx(float dip, Resources res) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip, res.getDisplayMetrics());
    }

    /**
     * Converts scaled pixels to pixels.
     * @param sp the number of SPs
     * @param res the resources to use
     * @return the equivalent value in pixels
     */
    public static float spToPx(float sp, Resources res) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, res.getDisplayMetrics());
    }

    /**
     * Converts a meters per second value to kilometers per hour
     * @param mps the mps value to convert to kph
     * @return the converted value in kph
     */
    public static float mpsToKph(float mps) {
        return mps * 3.6f;
    }
}
