package au.com.adapptor.helpers.android.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatSpinner;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import au.com.adapptor.helpers.R;

import static android.support.annotation.Dimension.SP;

/**
 * A modified {@link Spinner} that doesn't automatically select the first entry in the list.
 */
public class NoDefaultSpinner extends AppCompatSpinner {
    /** The class tag. */
    private static final String TAG = "NoDefaultSpinner";

    /** The hint text to display */
    private String mHintText = "";

    /** The colour of the hint text */
    private int mHintColor = Color.GRAY;

    /** Font for the hint */
    private Typeface mHintFont;

    /** The text size of the hint */
    private float mHintTextSizeSp;

    /** Flag which represents if we took the font value from a text input (true) or from a dimen
     * resource (false) */
    private boolean mFontValueFromText;

    /**
     * Constructor.
     * @param context the current context
     */
    public NoDefaultSpinner(Context context) {
        super(context);
    }

    /**
     * Constructor.
     * @param context the current context
     * @param attrs the attributes that are being used to create this control
     */
    public NoDefaultSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
        setUpAttributes(context, attrs);
    }

    /**
     * Constructor.
     * @param context the current context
     * @param attrs the attributes that are being used to create this control
     * @param defStyle the default style values reference
     */
    public NoDefaultSpinner(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setUpAttributes(context, attrs);
    }

    /**
     * Gets attributes for the hint
     * @param context app context
     * @param attrs attributes
     */
    private void setUpAttributes(Context context, AttributeSet attrs) {
        mHintText = attrs.getAttributeValue(
            "http://schemas.android.com/apk/res/android",
            "hint");

        if (mHintText != null && mHintText.startsWith("@")) {
            int stringRes = attrs.getAttributeResourceValue(
                "http://schemas.android.com/apk/res/android",
                "hint", R.string.empty);
            mHintText = context.getString(stringRes);
        }

        final int hintColourResVal = attrs.getAttributeResourceValue(
            "http://schemas.android.com/apk/res/android",
            "textColorHint",
            R.color.default_hint_grey);
        mHintColor = context.getResources().getColor(hintColourResVal);

        String textSizeStr = attrs.getAttributeValue(
            "http://schemas.android.com/apk/res/android",
            "textSize");

        if (textSizeStr != null) {
            if (textSizeStr.startsWith("@")) {
                int textSizeRes = attrs.getAttributeResourceValue(
                    "http://schemas.android.com/apk/res/android",
                    "textSize",
                    R.dimen.default_text_size);
                mHintTextSizeSp = context.getResources().getDimension(textSizeRes);
                mFontValueFromText = false;
            } else {
                textSizeStr = textSizeStr.replaceAll("[a-z]", "");
                mHintTextSizeSp = Float.parseFloat(textSizeStr);
                mFontValueFromText = true;
            }
        }

        setUpFont(attrs);
    }

    /**
     * Sets up font attribute for the hint
     * @param attrs attributes
     */
    private void setUpFont(AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.FontTextViewBase);
            String fontName = a.getString(R.styleable.FontTextViewBase_textFont);

            if (fontName != null && !isInEditMode()) {
                mHintFont = Fonts.getInstance().getTypeface("fonts/" + fontName);
            }

            a.recycle();
        }
    }

    @Override
    @SuppressLint("PrivateApi")
    public void setAdapter(SpinnerAdapter orig) {
        super.setAdapter(newProxy(orig));

        try {
            final Method m = AdapterView.class.getDeclaredMethod("setNextSelectedPositionInt", int.class);
            m.setAccessible(true);
            m.invoke(this, -1);

            final Method n = AdapterView.class.getDeclaredMethod("setSelectedPositionInt", int.class);
            n.setAccessible(true);
            n.invoke(this, -1);
        } catch (Exception e) {
            Log.e(TAG, "Error setting the initial adapter position to -1.", e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Creates a new adapter proxy.
     * @param adapter the underlying adapter
     * @return a new adapter proxy
     */
    protected SpinnerAdapter newProxy(SpinnerAdapter adapter) {
        return (SpinnerAdapter) java.lang.reflect.Proxy.newProxyInstance(
            adapter.getClass().getClassLoader(),
            new Class[]{SpinnerAdapter.class},
            new SpinnerAdapterProxy(adapter));
    }

    /**
     * A {@link Spinner} adapter that intercepts {@code getView} to display a blank entry
     * or a hint when position < 0.
     */
    protected class SpinnerAdapterProxy implements InvocationHandler {
        /** The underlying adapter. */
        protected SpinnerAdapter mAdapter;

        /** The underlying adapter's {@code getView} method. */
        protected Method mGetView;

        /**
         * Constructor.
         * @param adapter the underlying adapter
         */
        protected SpinnerAdapterProxy(SpinnerAdapter adapter) {
            mAdapter = adapter;

            try {
                mGetView = SpinnerAdapter.class.getMethod("getView", int.class, View.class, ViewGroup.class);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }

        /**
         * The invoke method that will be called when methods to the underlying adapter are called.
         * @param proxy the proxy object
         * @param m the method that is being called
         * @param args the arguments to the method
         * @return a blank view when appropriate, else the result of calling the underlying adapter
         * @throws IllegalAccessException
         * @throws InvocationTargetException
         */
        public Object invoke(Object proxy, Method m, Object[] args)
            throws IllegalAccessException, InvocationTargetException {

            if (m.equals(mGetView)) {
                if ((int) args[0] < 0) {
                    final ViewGroup parent = (ViewGroup) args[2];
                    final LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                    final View view = inflater.inflate(android.R.layout.simple_spinner_item, parent, false);
                    TextView hint = (TextView)view.findViewById(android.R.id.text1);
                    if (hint != null) {
                        hint.setText(mHintText);
                        hint.setTextColor(mHintColor);
                        hint.setTextSize(
                            mFontValueFromText ? SP : TypedValue.COMPLEX_UNIT_PX,
                            mHintTextSizeSp);
                        if (mHintFont != null) {
                            hint.setTypeface(mHintFont);
                        }
                    }
                    return view;
                }
            }

            return m.invoke(mAdapter, args);
        }
    }
}
