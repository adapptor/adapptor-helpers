package au.com.adapptor.helpers.android.helpers;

import android.app.Activity;
import android.os.Handler;
import android.os.Message;

import java.util.Vector;

/**
 * Message Handler class that supports buffering up of messages when the
 * activity is paused i.e. in the background.
 *
 * http://stackoverflow.com/questions/8040280/how-to-handle-handler-messages-when-activity-fragment-is-paused
 */
@SuppressWarnings("unused")
public abstract class PauseHandler extends Handler {
    /** Message queue buffer. */
    private final Vector<Message> mMessageQueueBuffer = new Vector<>();

    /** Flag indicating the pause state. */
    private boolean mPaused;

    /** {@code Activity} instance. */
    protected Activity mActivity;

    /**
     * Set the activity associated with the handler.
     * @param activity the mActivity to set
     */
    public void setActivity(Activity activity) {
        mActivity = activity;
    }

    /**
     * Resume the handler.
     */
    final public void resume() {
        mPaused = false;

        while (mMessageQueueBuffer.size() > 0) {
            final Message msg = mMessageQueueBuffer.elementAt(0);
            mMessageQueueBuffer.removeElementAt(0);
            sendMessage(msg);
        }
    }

    /**
     * Pause the handler.
     */
    final public void pause() {
        mPaused = true;
    }

    /**
     * Notification that the message is about to be stored as the activity is
     * paused. If not handled the message will be saved and replayed when the
     * activity resumes.
     *
     * @param message the message which optional can be handled
     * @return true if the message is to be stored
     */
    @SuppressWarnings({"SameReturnValue", "UnusedParameters"})
    protected boolean storeMessage(Message message) {
        return true;
    }

    /**
     * Notification message to be processed. This will either be directly from
     * handleMessage or played back from a saved message when the activity was
     * paused.
     *
     * @param message the message to be handled
     */
    protected abstract void processMessage(Message message);

    @Override
    final public void handleMessage(Message msg) {
        if (mPaused) {
            if (storeMessage(msg)) {
                Message msgCopy = new Message();
                msgCopy.copyFrom(msg);
                mMessageQueueBuffer.add(msgCopy);
            }
        } else {
            processMessage(msg);
        }
    }
}
