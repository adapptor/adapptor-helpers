package au.com.adapptor.helpers.android.view;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckedTextView;
import android.util.AttributeSet;

@SuppressWarnings("unused")
public class FontCheckedTextView extends AppCompatCheckedTextView implements FontTextViewBase {
    public FontCheckedTextView(Context context) {
        super(context);
        init(null);
    }

    public FontCheckedTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FontCheckedTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }
}
