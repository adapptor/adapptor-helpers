package au.com.adapptor.helpers.android.controller;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.io.Serializable;

/**
 * A {@link DialogFragment} that enables the user to select a date.
 */
@SuppressWarnings("unused")
public class DatePickerFragment extends DialogFragment
    implements DatePickerDialog.OnDateSetListener {

    /** Bundle ID for the listener argument. */
    private static final String kArgListener = "listener";

    /** Bundle ID for the year argument. */
    private static final String kArgYear = "year";

    /** Bundle ID for the month argument. */
    private static final String kArgMonth = "month";

    /** Bundle ID for the date argument. */
    private static final String kArgDate = "date";

    /** The listener. */
    private Listener mListener;

    /**
     * Defines this fragment's callback interface.
     */
    public interface Listener extends Serializable {
        /**
         * Informs the observer that the user has selected a new date.
         * @param year the selected year
         * @param month the selected month
         * @param date the selected date
         */
        void onDateSet(int year, int month, int date);
    }

    /**
     * Creates a new {@code DatePickerFragment} instance.
     * @param year the initially selected year
     * @param month the initially selected month
     * @param date the initially selected date
     * @param listener the listener
     * @return the new fragment
     */
    public static DatePickerFragment newInstance(int year, int month, int date, Listener listener) {
        final DatePickerFragment fragment = new DatePickerFragment();

        final Bundle args = new Bundle();
        args.putInt(kArgYear, year);
        args.putInt(kArgMonth, month);
        args.putInt(kArgDate, date);
        args.putSerializable(kArgListener, listener);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();

        mListener = (Listener) args.getSerializable(kArgListener);

        return new DatePickerDialog(getActivity(), this,
            args.getInt(kArgYear), args.getInt(kArgMonth), args.getInt(kArgDate));
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int date) {
        mListener.onDateSet(year, month, date);
    }
}
