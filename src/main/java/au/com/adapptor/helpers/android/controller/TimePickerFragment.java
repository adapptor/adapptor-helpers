package au.com.adapptor.helpers.android.controller;

import android.app.Dialog;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.format.DateFormat;
import android.widget.TimePicker;

import java.io.Serializable;

/**
 * A {@link DialogFragment} that enables the user to select a time.
 */
@SuppressWarnings("unused")
public class TimePickerFragment extends DialogFragment
    implements TimePickerDialog.OnTimeSetListener {

    /** Bundle ID for the listener argument. */
    private static final String kArgListener = "listener";

    /** Bundle ID for the hour argument. */
    private static final String kArgHour = "hour";

    /** Bundle ID for the minute argument. */
    private static final String kArgMinute = "minute";

    /** The listener. */
    private Listener mListener;

    /**
     * Defines this fragment's callback interface.
     */
    public interface Listener extends Serializable {
        /**
         * Informs the observer that the user has selected a new time.
         * @param hour the selected hour
         * @param minute the selected minute
         */
        void onTimeSet(int hour, int minute);
    }

    /**
     * Creates a new {@code TimePickerFragment} instance.
     * @param hour the initially selected hour
     * @param minute the initially selected minute
     * @param listener the listener
     * @return the new fragment
     */
    public static TimePickerFragment newInstance(int hour, int minute, Listener listener) {
        final TimePickerFragment fragment = new TimePickerFragment();

        final Bundle args = new Bundle();
        args.putInt(kArgHour, hour);
        args.putInt(kArgMinute, minute);
        args.putSerializable(kArgListener, listener);

        fragment.setArguments(args);
        return fragment;
    }

    @Override
    @NonNull
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Bundle args = getArguments();

        mListener = (Listener) args.getSerializable(kArgListener);

        return new TimePickerDialog(getActivity(), this,
            args.getInt(kArgHour), args.getInt(kArgMinute),
            DateFormat.is24HourFormat(getActivity()));
    }

    public void onTimeSet(TimePicker view, int hour, int minute) {
        mListener.onTimeSet(hour, minute);
    }
}
