package au.com.adapptor.helpers.android.helpers;

import com.google.android.gms.maps.model.LatLng;

/**
 * Contains helper methods for converting between various forms of location classes.
 */
@SuppressWarnings("unused")
public class LocationHelper {
    /**
     * Create a {@link LatLng} object from a {@link android.location.Location}.
     * @param location the {@code Location} to convert
     * @return the equivalent {@code LatLng}
     */
    public static LatLng locationToLatLng(android.location.Location location) {
        return location == null ? null : new LatLng(location.getLatitude(), location.getLongitude());
    }

    /**
     * Create a {@link LatLng} object from a {@link au.com.adapptor.helpers.universal.Location}.
     * @param location the {@code Location} to convert
     * @return the equivalent {@code LatLng}
     */
    public static LatLng locationToLatLng(au.com.adapptor.helpers.universal.Location location) {
        return location == null ? null : new LatLng(location.latitude, location.longitude);
    }
}
