package au.com.adapptor.helpers.android.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

// Cache typeface loading
@SuppressWarnings("unused")
public class Fonts {
    private static final String TAG = "Fonts";

    @SuppressLint("StaticFieldLeak")
    private static Fonts sInstance;

    private final Context mContext;
    private final Map<String, Typeface> mFonts = new HashMap<>();
    private String mDefaultTypefacePath;

    private Fonts(Context context) {
        mContext = context;
    }

    public static void create(Context context) {
        sInstance = new Fonts(context);
    }

    public static Fonts getInstance() {
        return sInstance;
    }

    public Typeface getTypeface(String path) {
        if (!mFonts.containsKey(path)) {
            try {
                mFonts.put(path, Typeface.createFromAsset(mContext.getAssets(), path));
            } catch (Exception ex) {
                Log.e(TAG, "Error creating typeface: " + path);
            }
        }

        return mFonts.get(path);
    }

    public Typeface getDefaultTypeface() {
        return getTypeface(mDefaultTypefacePath);
    }

    public void setDefaultTypefacePath(String path) {
        mDefaultTypefacePath = path;
    }
}
