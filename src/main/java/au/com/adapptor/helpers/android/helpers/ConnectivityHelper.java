package au.com.adapptor.helpers.android.helpers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Helper class to check connectivity to the internet
 */
public class ConnectivityHelper {

    public static final String TAG = "ConnectivityHelper";

    /**
     * Checks if an internet connection can be found
     * @param context the context of the app
     * @return true if there is a internet connection, false otherwise
     */
    public static boolean isConnectedToInternet(Context context) {
        ConnectivityManager cm =
            (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    /**
     * Checks if the device is connected to wifi or not
     * @param context the context of the app
     * @return true if the device is connected to wifi, false otherwise
     */
    public static boolean isConnectedToWifi(Context context) {
        ConnectivityManager cm =
            (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;
    }

    /**
     * Observes connectivity events
     */
    public interface ConnectivityListener {
        /**
         * notifies the observer that we have either connected or disconnected from the internet
         * @param connected true if we are connected to the internet, false otherwise
         */
        void onConnectionChanged(boolean connected);
    }

    /** context of the activity using this helper instance */
    private Context mContext;
    /** Receiver of broadcasted connectivity intents */
    private BroadcastReceiver mConnectivityReceiver;
    /** Handle to the observer of connectivity  */
    private ConnectivityListener mConnectivityListener;
    /** flag that keeps track of whether we have registered the broadcast reveiver or not */
    private boolean mReceiverRegistered = false;

    /**
     * Constructor
     * @param context the context of the app
     * @param listener  observer of conenctivity events
     */
    public ConnectivityHelper(Context context, ConnectivityListener listener) {
        mContext = context;
        mConnectivityListener = listener;
    }

    /**
     * sets up a broadcast receiver to listen for changes in connectivity.
     * @apiNote Should be called in onResume()
     */
    public synchronized void listenForConnectivityChanges() {
        if (mReceiverRegistered) {
            return;
        }

        IntentFilter filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        mConnectivityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                handleConnectivityChange();
            }
        };
        mContext.registerReceiver(mConnectivityReceiver, filter);
        mReceiverRegistered = true;
    }

    /**
     * unregisters the broadcast receiver
     * * @apiNote Should be called in onPause()
     */
    public void stopListeningForConnectivityChanges() {
        try {
            mContext.unregisterReceiver(mConnectivityReceiver);
            mReceiverRegistered = false;
        } catch (IllegalArgumentException e) {
            Log.e(TAG, "error occurred when unregistering connectivity receiver: "
                + e.getMessage());
        }
    }

    /**
     * Handles a change in connectivity
     */
    private void handleConnectivityChange() {
        try {
            boolean isConnected = ConnectivityHelper.isConnectedToInternet(mContext);
            if (mConnectivityListener != null) {
                mConnectivityListener.onConnectionChanged(isConnected);
            }

        } catch (IllegalArgumentException e) {
            Log.e(TAG, "error occurred when registering or unregistering connectivity receiver: "
                + e.getMessage());
        }
    }



}
