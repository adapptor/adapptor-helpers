package au.com.adapptor.helpers.android.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Manages shared preferences for the application.
 */
@SuppressWarnings({"unused", "StaticFieldLeak"})
public class Prefs {
    /** A context used for global shared preferences. */
    private static Context sContext;

    /**
     * Constructor. Instances are obtained via {@link #local(Context)} and {@link #global()}.
     */
    protected Prefs() {
    }

    /**
     * Initialisation method. This sets the context to be used for the global shared preferences.
     * @param context context for the global shared preferences
     */
    public static void init(Context context) {
        sContext = context;
    }

    /**
     * Get shared preferences with a supplied context - for local preferences.
     * @param context the context to be used
     * @return local shared preferences
     */
    public static SharedPreferences local(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    /**
     * Get shared preferences using our saved context - for global preferences.
     * @return global shared preferences
     */
    public static SharedPreferences global() {
        return PreferenceManager.getDefaultSharedPreferences(sContext);
    }
}
