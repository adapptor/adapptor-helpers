package au.com.adapptor.helpers.android.view;

import android.content.Context;
import android.support.v7.widget.AppCompatButton;
import android.util.AttributeSet;

@SuppressWarnings("unused")
public class FontButton extends AppCompatButton implements FontTextViewBase {
    public FontButton(Context context) {
        super(context);
        init(null);
    }

    public FontButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FontButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs);
    }
}
