package au.com.adapptor.helpers.android.helpers;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

/**
 * Methods for dealing with {@code Bitmap}s, {@code Drawable}s, etc.
 */
@SuppressWarnings("unused")
public class GraphicsHelper {
    /**
     * Converts a {@code Drawable} to a {@code Bitmap}. If {@code drawable} is an instance of {@code BitmapDrawable}
     * and it is of the requested size, then {@code drawable}'s bitmap is returned.
     * @param drawable the {@code Drawable} to convert
     * @param width the width of the resulting bitmap
     * @param height the height of the resulting bitmap
     * @return the new bitmap
     */
    public static Bitmap bitmapFromDrawable(Drawable drawable, int width, int height) {
        if (drawable instanceof BitmapDrawable) {
            final Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();

            if (width == bitmap.getWidth() && height == bitmap.getHeight()) {
                return bitmap;
            }
        }

        final Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);

        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);

        return bitmap;
    }

    /**
     * Returns a version of the supplied {@code Bitmap} with the requested size.
     * If the passed in {@code Bitmap} is already of the correct size, it is simply returned.
     * @param bitmap the {@code Bitmap} to resize
     * @param width the desired width
     * @param height the desired height
     * @param filter whether or not the filter should be used
     * @return the resized {@code Bitmap}
     */
    public static Bitmap resizeBitmap(Bitmap bitmap, int width, int height, boolean filter) {
        return width != bitmap.getWidth() || height != bitmap.getHeight()
            ? Bitmap.createScaledBitmap(bitmap, width, height, filter)
            : bitmap;
    }

    /**
     * Returns a version of the supplied {@code Drawable} with the requested size.
     * @param drawable the {@code Drawable} to resize
     * @param width the desired width
     * @param height the desired height
     * @param res the resources to use
     * @return the resized {@code Drawable}
     */
    public static Drawable resizeDrawable(Drawable drawable, int width, int height, Resources res) {
        return new BitmapDrawable(res, bitmapFromDrawable(drawable, width, height));
    }
}
